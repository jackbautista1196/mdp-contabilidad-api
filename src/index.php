<?php

    include("ClassLocatarios.php");
    $action = @$_REQUEST['action'] ? $_REQUEST['action'] : '';
    $startExport = @$_REQUEST['start'] ? $_REQUEST['start'] : '';
    $endExport = @$_REQUEST['end'] ? $_REQUEST['end'] : '';
    $idRequest = @$_REQUEST['id'] ? $_REQUEST['id'] : '';
    $host = @$_POST['hostname'] ? $_POST['hostname'] : '';
    $user = @$_POST['user'] ? $_POST['user'] : '';
    $password = @$_POST['password'] ? $_POST['password'] : '';
    $dbname = @$_POST['dbname'] ? $_POST['dbname'] : '';
    $startDate = @$_POST['startDate'] ? $_POST['startDate'] : '';
    $endDate = @$_POST['endDate'] ? $_POST['endDate'] : '';
    $idSupply = @$_POST['idSupply'] ? $_POST['idSupply'] : '';
    $idCuenta = @$_POST['idCuenta'] ? $_POST['idCuenta'] : '';
    $user_email = @$_POST['user_email'] ? $_POST['user_email'] : '';
    $user_password = @$_POST['user_password'] ? $_POST['user_password'] : '';
    $idTurn = @$_POST['id_turn'] ? $_POST['id_turn'] : '';
    $idBox = @$_POST['id_box'] ? $_POST['id_box'] : '';
    $idRegbox = @$_POST['id_regbox'] ? $_POST['id_regbox'] : '';
    $idArea = @$_REQUEST['id_area'] ? $_REQUEST['id_area'] : 0;
    $idTypeSale = @$_REQUEST['id_type_sale'] ? $_REQUEST['id_type_sale'] : 0;
    $idCategory = @$_REQUEST['id_category'] ? $_REQUEST['id_category'] : 0;
    $idProf = @$_REQUEST['id_prof'] ? $_REQUEST['id_prof'] : 0;
    $id = '';
    $dataLocal = '';
    $all = false;
    $locals = [
        ['id' => 12, 'hostname' => '134.209.163.214', 'password' => '0mBe*Y3TS@Qf0cLn$', 'user' => 'root', 'database' => 'mdp_picogallo', 'reason' => 'PICOGALLO S.A.C.', 'ruc' => '20603993234', 'name' => 'PICOGALLO'],
        ['id' => 11, 'hostname' => '134.122.24.161', 'password' => '9VjDFW3jyyzA2xEU', 'user' => 'root', 'database' => 'mdp_kilo', 'reason' => 'ACM INVERSIONES SAC', 'ruc' => '20605113584', 'name' => 'KILO'],
        ['id' => 9, 'hostname' => '134.122.24.161', 'password' => '9VjDFW3jyyzA2xEU', 'user' => 'root', 'database' => 'mdp_3nortes', 'reason' => 'TRES NORTES SAC', 'ruc' => '20603359004', 'name' => 'TRES NORTES'],
        ['id' => 8, 'hostname' => '134.122.24.161', 'password' => '9VjDFW3jyyzA2xEU', 'user' => 'root', 'database' => 'mdp_la_picante', 'reason' => 'AYB CONCESIONARIOS SAC', 'ruc' => '20600789636', 'name' => 'LA PICANTE'],
        ['id' => 7, 'hostname' => '134.122.24.161', 'password' => '9VjDFW3jyyzA2xEU', 'user' => 'root', 'database' => 'mdp_boa', 'reason' => 'PEDRO MIGUEL SAV', 'ruc' => '20507660712', 'name' => 'BOA'],
        ['id' => 6, 'hostname' => '134.122.24.161', 'password' => '9VjDFW3jyyzA2xEU', 'user' => 'root', 'database' => 'mdp_vivawok', 'reason' => 'WOK EIRL', 'ruc' => '20603993358', 'name' => 'VIVAWOK'],
        ['id' => 5, 'hostname' => '134.122.24.161', 'password' => '9VjDFW3jyyzA2xEU', 'user' => 'root', 'database' => 'mdp_ino', 'reason' => 'SUCRÉ SALÉ S.A.C.', 'ruc' => '20601011175', 'name' => 'INO'],
        ['id' => 4, 'hostname' => '134.122.24.161', 'password' => '9VjDFW3jyyzA2xEU', 'user' => 'root', 'database' => 'mdp_lateria', 'reason' => 'PATIO GASTRONOMICO SAC', 'ruc' => '20603867689', 'name' => 'PALETAS'],
        ['id' => 3, 'hostname' => '134.122.24.161', 'password' => '9VjDFW3jyyzA2xEU', 'user' => 'root', 'database' => 'mdp_sburger', 'reason' => 'SANGUCHERIA REAL SAC', 'ruc' => '20605901868', 'name' => 'BURGER'],
        ['id' => 2, 'hostname' => '134.122.24.161', 'password' => '9VjDFW3jyyzA2xEU', 'user' => 'root', 'database' => 'mdp_bar_del_pilar', 'reason' => 'PATIO GASTRONOMICO SAC', 'ruc' => '20603867689', 'name' => 'BAR DEL PILAR']
        
    ];
    if ($idRequest != 0) {
        foreach ($locals as $local) {
            if ($local['id'] == $idRequest) {
                $dataLocal = $local;
            }
        }
    } else {
        $all = true;
    }

    $locatarios = new ClassLocatarios();

    switch ($action) {
    case 'get_user' :
    {
        $locatarios->getUser($user_email, $user_password);
        break;
    }
    case 'get_locatarios' :
    {
        $locatarios->getLocatarios();
        break;
    }
    case 'get_data_transactions':
    {
        $locatarios->getDataLocatarios($host, $dbname, $user, $password, $startDate, $endDate);
        break;
    }
    case 'get_data_graphics_transactions':
    {
        $locatarios->getDataGraphicsLocatarios($host, $dbname, $user, $password, $startDate, $endDate);
        break;
    }
    case 'get_data_inventory':
    {
        $locatarios->getDataInventorybyLocal($host, $dbname, $user, $password);
        break;
    }
    case 'get_data_kardex':
    {
        $locatarios->getKardexByLocal($host, $dbname, $user, $password, $startDate, $endDate, $idSupply);
        break;
    }
    case 'get_supplies':
    {
        $locatarios->getSupplies($host, $dbname, $user, $password);
        break;
    }
    case 'get_cuentas_por_cobrar':
    {
        $locatarios->getCuentasPorCobrar($host, $dbname, $user, $password);
        break;
    }
    case 'get_data_cuentas':
    {
        $locatarios->getDataCuentasPorCobrar($host, $dbname, $user, $password, $startDate, $endDate, $idCuenta);
        break;
    }
    case 'get_cajas':
    {
        $locatarios->getCajas($host, $dbname, $user, $password);
        break;
    }
    case 'get_turns':
    {
        $locatarios->getDataTurns($host, $dbname, $user, $password);
        break;
    }
    case 'get_cierres':
    {
        $locatarios->getCierres($host, $dbname, $user, $password, $idBox, $idTurn);
        break;
    }
    case 'get_search_caja':
    {
        $locatarios->getSearchCaja($host, $dbname, $user, $password, $idRegbox);
        break;
    }
    case 'get_platos_horas_punta':
    {
        $locatarios->getPlatosTopTime($host, $dbname, $user, $password, $startDate, $endDate);
        break;
    }
    case 'boxes':
    {
        $locatarios->boxes($host, $dbname, $user, $password);
        break;
    }
    case 'export_sales':
    {
        if ($all) {
            $locatarios->exportAllSales($locals, $startExport, $endExport);
        } else {
            $locatarios->exportSales($dataLocal, $startExport, $endExport);
        }
        break;
    }
    case 'get_areas':
    {
        $locatarios->getAreas($host, $dbname, $user, $password);
        break;
    }
    case 'get_categories':
    {
        $locatarios->getCategories($host, $dbname, $user, $password);
        break;
    }
    case 'get_profs':
    {
        $locatarios->getProfs($host, $dbname, $user, $password);
        break;
    }
    case 'export_sales_by_plate':
    {
        $locatarios->exportSalesByPlate($dataLocal, $startExport, $endExport, $idArea, $idTypeSale, $idCategory, $idProf);
        break;
    }
    case 'export_most_selled':
    {
        $locatarios->most_selled($dataLocal, $startExport, $endExport);
        break;
    }
}
