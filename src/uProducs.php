<?php


/**
 * Class UProd
 * Funciones utiles relacionado con productos
 */
class UProd
{

    public function __construct()
    {
    }

    /**
     * Obtener costo real de Presentacion de producto, para una venta
     */
    public static function getRealCostPropre($id_propre, $id_ordpro, $return_html = false)
    {
        $cone = new PDO("mysql:host=localhost;dbname=mdp_rest1", "root", "");
        global $db;

        $html_sus = '';
        $cost_total = 0;

        $SQL = $cone->prepare("SELECT ps.*,
                   su.id    su_id,
                   su.name  su_name,
                   su.cost  su_cost,
                   
                   un1.id   un1_id,
                   un1.name un1_name,
                   un2.id   un2_id,
                   un2.name un2_name,
                   
                   COALESCE(ka.v_unit,su.cost) real_cost,
                   
                   ka.v_unit ka_v_unit
                   
            FROM propresups ps
            
              INNER JOIN supplies su ON su.id = ps.id_supply
                LEFT JOIN kardex ka ON ka.id_supply = su.id AND ka.type = 2 AND ka.id_ordpro = $id_ordpro
                
              LEFT JOIN unimeds un1 ON un1.id = su.id_unimed
              LEFT JOIN unimeds un2 ON un2.id = ps.id_unimed
              
            WHERE ps.id_propre = $id_propre
            GROUP BY ps.id");
        $SQL->execute();
        while ($ps = $SQL->fetch(PDO::FETCH_OBJ)) {

            // Obtener costo
            $SQL_1 = $cone->prepare("SELECT *
                  FROM unimeds_rel
                  WHERE state = 1 AND (
                        (id_unimed_org = $ps->un1_id AND id_unimed_dst = $ps->un2_id) OR
                        (id_unimed_dst = $ps->un1_id AND id_unimed_org = $ps->un2_id)
                       )");
            $SQL_1->execute();
            $cost = 0;

            $css_status = '';

            if ($ps->un1_id == $ps->un2_id || $ur = $SQL_1->fetch(PDO::FETCH_OBJ)) {
                // Son la misma unidad de medida, no se debe relacionar
                if ($ps->un1_id == $ps->un2_id) {

                    $cost = $ps->real_cost * $ps->quantity;

                } else if ($ur->id_unimed_org == $ps->un1_id) {

                    $cost = ($ps->real_cost * $ps->quantity) / $ur->quantity;

                } else {
                    $cost = ((($ur->quantity / $ps->quantity) / $ps->real_cost) * $ps->quantity) * $ps->quantity;
                }
            } else {
                $css_status = 'color:#ddd';
            }

            $cost_total += $cost;

            if ($return_html) {
                $html_sus .= '<tr style="' . $css_status . '">';
                $html_sus .= ' <td width="20px"> </td>';
                $html_sus .= ' <td>' . $ps->quantity . '</td>';
                $html_sus .= ' <td>' . $ps->un2_name . '</td>';
                $html_sus .= ' <td>' . $ps->su_id . ' : ' . $ps->su_name . ' (S/ <b>' . $ps->real_cost . '</b> : ' . $ps->su_cost . ' (' . $ps->ka_v_unit . ') x ' . $ps->un1_name . ')</td>';
                $html_sus .= ' <td>' . $cost . '</td>';
                $html_sus .= '</tr>';
            }
        }
        header("Access-Control-Allow-Origin:*");
        header("Content-type: application/json");
        if ($return_html) {
            return '
                <table class="table table-bordered">
                    <tr>
                        <td colspan="4"><b> mmm - mmm</b></td>
                        <td><b>' . $cost_total . '</b></td>
                    </tr>
                    ' . $html_sus . '
                </table>
            ';
        } else {
            return $cost_total;
        }
    }


}