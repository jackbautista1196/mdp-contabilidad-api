<?php
include("ClassConnection.php");
include('uProducs.php');
require 'vendor/autoload.php';
ini_set("memory_limit", "256M");

use PhpOffice\PhpSpreadsheet\Chart\Chart;
use PhpOffice\PhpSpreadsheet\Chart\DataSeries;
use PhpOffice\PhpSpreadsheet\Chart\DataSeriesValues;
use PhpOffice\PhpSpreadsheet\Chart\Legend;
use PhpOffice\PhpSpreadsheet\Chart\PlotArea;
use PhpOffice\PhpSpreadsheet\Chart\Title;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ClassLocatarios extends ClassConnection
{
    public function _GET_DATE($name, $default_value = '')
    {
        $val = @trim(@$_GET[$name]);
        return !empty($val) && Util::isDate($val) ? $val : $default_value;
    }

    public function _GET($name, $default_value = '')
    {
        $val = @trim(@$_GET[$name]);
        return !empty($val) ? $val : $default_value;
    }

    public function getParentCategoryById($id_category)
    {
        $cone = new PDO("mysql:host=localhost;dbname=mdp_rest1", "root", "");
        $cone->query("SET NAMES 'utf8'");
        $o = $cone->prepare("SELECT * FROM categories WHERE id = $id_category");
        $o->execute();
        if ($o = $o->fetch(PDO::FETCH_OBJ)) {
            if ($o->id_parent == 0) {
                return $o;
            } else {
                return $this->getParentCategoryById($o->id_parent);
            }
        } else {
            return null;
        }
    }

    public function array_sort($array, $on, $order = SORT_ASC)
    {
        $new_array = array();
        $sortable_array = array();

        if (count($array) > 0) {
            foreach ($array as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $k2 => $v2) {
                        if ($k2 == $on) {
                            $sortable_array[$k] = $v2;
                        }
                    }
                } else {
                    $sortable_array[$k] = $v;
                }
            }

            switch ($order) {
                case SORT_ASC:
                    asort($sortable_array);
                    break;
                case SORT_DESC:
                    arsort($sortable_array);
                    break;
            }

            foreach ($sortable_array as $k => $v) {
                $new_array[$k] = $array[$k];
            }
        }

        return $new_array;
    }

    public function getLocatarios()
    {
        $fetchLocatarios = $this->connectBD()->prepare("SELECT * FROM locatarios where state = 1 ORDER BY id");
        $fetchLocatarios->execute();
        $i = 0;
        $locatariosJSON = [];
        while ($fetchLocal = $fetchLocatarios->fetch(PDO::FETCH_OBJ)) {
            $locatariosJSON[$i] = $fetchLocal;
            $i++;
        }

        header("Access-Control-Allow-Origin:*");
        header("Content-type: application/json");
        echo json_encode($locatariosJSON);
    }

    public function boxes($host, $db, $user, $pass)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fetchBoxes = $cone->prepare("SELECT bo.id,bo.name,
               re.id re_id,
               re.id_turn re_id_turn
        FROM boxes bo
          LEFT JOIN regboxes re ON re.id_box = bo.id AND re.state = 1
        WHERE bo.state = 1
        ORDER BY name");
            $fetchBoxes->execute();
            $i = 0;
            $boxesJSON = [];
            while ($fetchBox = $fetchBoxes->fetch(PDO::FETCH_OBJ)) {
                $boxesJSON['' . $fetchBox->id] = $fetchBox;
                $i++;
            }

            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($boxesJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getDataLocatarios($host, $db, $user, $pass, $startDate, $endDate)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            /*$fetchTransactions = $cone->prepare("
            SELECT tr.*,
               cl.name cl_name,
               pf.name pf_name
        FROM transactions tr
          INNER JOIN regboxes rb ON rb.id = tr.id_regbox
          LEFT JOIN clients cl ON cl.id = tr.id_client
          LEFT JOIN proofs pf ON pf.id = tr.id_proof
          LEFT JOIN regboxes re ON re.id = tr.id_regbox
        WHERE tr.id_branch = 1
              AND rb.date_added BETWEEN '$startDate' AND '$endDate'
        ORDER BY tr.id DESC");*/
            $fetchTransactions = $cone->prepare("
        SELECT tr.*,
               cl.name cl_name,
               pf.name pf_name
        FROM transactions tr
          LEFT JOIN clients cl ON cl.id = tr.id_client
          LEFT JOIN proofs pf ON pf.id = tr.id_proof
        WHERE tr.id_branch = 1 AND tr.state <> 0 
              AND (pf.id = 2 OR pf.id = 5)
              AND tr.date_added BETWEEN '$startDate' AND '$endDate'
        ORDER BY tr.id DESC");
            $fetchTransactions->execute();
            $i = 0;
            $transactionsJSON = [];
            while ($fetchTransaction = $fetchTransactions->fetch(PDO::FETCH_OBJ)) {
                $transactionsJSON[$i] = $fetchTransaction;
                $i++;
            }
            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($transactionsJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getDataGraphicsLocatarios($host, $db, $user, $pass, $startDate, $endDate)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $class = new ClassLocatarios();
            $fils = new stdClass();
            $fils->date_from = $class->_GET_DATE(
                'date_from', /*date('Y-m-01')*/
                date('2020-01-01 ', strtotime('yesterday'))
            );
            $fils->date_to = $class->_GET_DATE(
                'date_to', /*date('Y-m-d')*/
                date('2020-03-31', strtotime('yesterday'))
            );
            $fils->nocache = $class->_GET('nocache');

            $RPT = [];

            $RPT['date_generated'] = date('d/m/Y g:i:s A');

            // Dashboard
            $RPT['dash'] = [];
            $RPT['dash']['sales'] = 0;
            $RPT['dash']['sales_cash'] = 0;
            $RPT['dash']['sales_cash_pc'] = 0;
            $RPT['dash']['sales_card'] = 0;
            $RPT['dash']['sales_card_pc'] = 0;
            $RPT['dash']['sales_total'] = 0;
            $RPT['dash']['sales_total_persons'] = 0;
            $RPT['dash']['sales_total_transactions'] = 0;
            $RPT['dash']['sales_avg_table'] = 0;
            $RPT['dash']['sales_avg_person'] = 0;

            // Delivery
            $RPT['dash']['sales_deli'] = 0;
            $RPT['dash']['sales_deli_cash'] = 0;
            $RPT['dash']['sales_deli_cash_pc'] = 0;
            $RPT['dash']['sales_deli_card'] = 0;
            $RPT['dash']['sales_deli_card_pc'] = 0;

            $RPT['dash']['sales_total_valet_num'] = 0;
            $RPT['dash']['sales_total_valet_price'] = 0;

            $RPT['dash']['sales_total_courtesy_num'] = 0;
            $RPT['dash']['sales_total_courtesy_price'] = 0;

            $RPT['dash']['clients'] = 0;
            $RPT['dash']['clients_per'] = 0;
            $RPT['dash']['clients_per_pc'] = 0;
            $RPT['dash']['clients_emp'] = 0;
            $RPT['dash']['clients_emp_pc'] = 0;

            $RPT['dash']['avg_areas'] = [];

            $RPT['dash']['turns'] = [];

            // Promedio de consumo

            $SQL = $cone->prepare("SELECT COUNT(*) num_orders,
               od.id od_id,
               IF(od.num_persons=0,1,od.num_persons) od_num_persons
        FROM ordpros op
          INNER JOIN orders od ON od.id = op.id_order
          INNER JOIN transactions tr ON tr.id = op.id_transaction
            INNER JOIN regboxes rb ON rb.id = tr.id_regbox
        WHERE tr.state != 0 AND tr.type = 1
              AND DATE(rb.date_added) BETWEEN '$startDate' AND '$endDate'
        GROUP BY op.id_order");
            $SQL->execute();

            //exit($SQL);
            while ($o = $SQL->fetch(PDO::FETCH_ASSOC)) {
                //$RPT['dash']['sales_total']+= $o['num_orders'];
                $RPT['dash']['sales_total_persons'] += $o['od_num_persons'];
                $RPT['dash']['sales_total'] += 1;
            }

            // Promedio de tiempo despacho
            $SQL = $cone->prepare("SELECT SUM(TIMESTAMPDIFF(MINUTE,op.date_added,op.date_dispatched)) sec_diff,
               COUNT(op.id) total_items,
               ar.id ar_id,
               ar.name ar_name
        FROM ordpros op
          INNER JOIN propres pp ON pp.id = op.id_propre
            INNER JOIN products pr ON pr.id = pp.id_product
              INNER JOIN areas ar ON ar.id = pr.id_area
          INNER JOIN transactions tr ON tr.id = op.id_transaction
            INNER JOIN regboxes rb ON rb.id = tr.id_regbox
        WHERE tr.state != 0 AND tr.type = 1
              AND DATE(rb.date_added) BETWEEN '$startDate' AND '$endDate'
        GROUP BY ar.id");
            $SQL->execute();
            while ($o = $SQL->fetch(PDO::FETCH_OBJ)) {

                $RPT['dash']['avg_areas'][] = [
                    'name' => $o->ar_name,
                    'total_items' => $o->total_items,
                    'total_secs' => $o->sec_diff,
                    'avg_secs' => 0
                ];
            }

            foreach ($RPT['dash']['avg_areas'] as &$a) {
                $a['avg_secs'] = round($a['total_secs'] / $a['total_items']);
            }

            // Reporte diario
            $SQL = $cone->prepare("SELECT SUM(tr.total) value,
               SUM(tr.cash) total_cash,
               SUM(tr.card) total_card,
               DATE(tr.date_added) date
        FROM transactions tr
          INNER JOIN regboxes rb ON rb.id = tr.id_regbox
        WHERE tr.state != 0 AND tr.type = 1
              AND DATE(rb.date_added) BETWEEN '$startDate' AND '$endDate'
        GROUP BY DATE(tr.date_added)");
            $RPT['data_daily'] = [];
            $SQL->execute();
            while ($o = $SQL->fetch(PDO::FETCH_ASSOC)) {
                //$o['value'] = number_format($o['value'], 2, '.', '');
                $RPT['data_daily'][] = $o;
            }

            // Por turnos
            $SQL = $cone->prepare("SELECT tr.*,

               tu.id tu_id,
               tu.name tu_name,

               od.num_persons od_num_persons,

               pf.code pf_code

        FROM transactions tr
          JOIN regboxes rb ON rb.id = tr.id_regbox
            JOIN turns tu ON tu.id = rb.id_turn
          JOIN orders od ON od.id = tr.id_ref
          JOIN proofs pf ON pf.id = tr.id_proof
        WHERE tr.state != 0 AND tr.type = 1
              AND DATE(rb.date_added) BETWEEN '$startDate' AND '$endDate'");
            //$RPT['data_daily'] = [];
            $SQL->execute();
            while ($o = $SQL->fetch(PDO::FETCH_ASSOC)) {

                // VENTAS
                $RPT['dash']['sales'] += $o['total'];
                $RPT['dash']['sales_cash'] += $o['cash'];
                $RPT['dash']['sales_card'] += $o['card'];
                $RPT['dash']['sales_total_transactions'] += 1;

                if ($o['is_deli']) {
                    $RPT['dash']['sales_deli'] += $o['total'];
                    $RPT['dash']['sales_deli_cash'] += $o['cash'];
                    $RPT['dash']['sales_deli_card'] += $o['card'];
                }

                $RPT['dash']['sales_total_valet_num'] += $o['valet_num'];
                $RPT['dash']['sales_total_valet_price'] += $o['valet_price'];

                // CORTESIAS
                if ($o['pf_code'] == '00') {
                    $RPT['dash']['sales_total_courtesy_num'] += 1;
                    $RPT['dash']['sales_total_courtesy_price'] += $o['total'];
                }

                // CLIENTES
                $RPT['dash']['clients'] += ($o['od_num_persons'] <= 0 ? 1 : $o['od_num_persons']);
                if ($o['pf_code'] == '01') {
                    $RPT['dash']['clients_emp'] += 1;
                } else if ($o['pf_code'] == '03') {
                    $RPT['dash']['clients_per'] += 1;
                }

                // TURNOS
                $key_turn = 'turn_' . $o['tu_id'];
                if (!isset($RPT['dash']['turns'][$key_turn])) {
                    $RPT['dash']['turns'][$key_turn] = [
                        'name' => $o['tu_name'],
                        'total' => 0,
                        'total_pc' => 0,

                        'total_deli' => 0,
                        'total_deli_pc' => 0
                    ];
                }
                $RPT['dash']['turns'][$key_turn]['total'] += $o['total'];

                if ($o['is_deli']) {
                    $RPT['dash']['turns'][$key_turn]['total_deli'] += $o['total'];
                }
            }

            foreach ($RPT['dash']['turns'] as &$c) {
                if ($c['total'] > 0) {
                    $c['total_pc'] = round(($c['total'] / $RPT['dash']['sales']) * 100);
                }
                if ($c['total_deli'] > 0) {
                    $c['total_deli_pc'] = round(($c['total_deli'] / $RPT['dash']['sales_deli']) * 100);
                }
            }

            // Modify Dashboard
            if ($RPT['dash']['sales_total_transactions'] > 0) {
                $RPT['dash']['clients_emp_pc'] = round(($RPT['dash']['clients_emp'] / $RPT['dash']['sales_total_transactions']) * 100);
                $RPT['dash']['clients_per_pc'] = round(($RPT['dash']['clients_per'] / $RPT['dash']['sales_total_transactions']) * 100);
            }


            // Modify Dashboard
            if ($RPT['dash']['sales'] > 0) {
                $RPT['dash']['sales_cash_pc'] = round(($RPT['dash']['sales_cash'] / $RPT['dash']['sales']) * 100);
                $RPT['dash']['sales_card_pc'] = round(($RPT['dash']['sales_card'] / $RPT['dash']['sales']) * 100);
            }

            if ($RPT['dash']['sales_deli'] > 0) {
                $RPT['dash']['sales_deli_cash_pc'] = round(($RPT['dash']['sales_deli_cash'] / $RPT['dash']['sales_deli']) * 100);
                $RPT['dash']['sales_deli_card_pc'] = round(($RPT['dash']['sales_deli_card'] / $RPT['dash']['sales_deli']) * 100);
            }

            if ($RPT['dash']['sales_total'] > 0) {
                $RPT['dash']['sales_avg_table'] = $RPT['dash']['sales'] / $RPT['dash']['sales_total'];
            }

            if ($RPT['dash']['sales_total_persons'] > 0) {
                $RPT['dash']['sales_avg_person'] = $RPT['dash']['sales'] / $RPT['dash']['sales_total_persons'];
            }

            // Mas vendidos
            $SQL = $cone->prepare("SELECT op.*,

               pp.id    pp_id,
               pp.name  pp_name,
               pp.cost  pp_cost,
               pp.price pp_price,

               pr.id    pr_id,
               pr.name  pr_name,

               ca.id        ca_id,
               ca.id_parent ca_id_parent,
               ca.name      ca_name,

               tr.pc_igv      tr_pc_igv,
               tr.pc_service  tr_pc_service

        FROM ordpros op
          INNER JOIN propres pp ON pp.id = op.id_propre
            INNER JOIN products pr ON pr.id = pp.id_product
              INNER JOIN categories ca ON ca.id = pr.id_category
          INNER JOIN transactions tr ON tr.id = op.id_transaction
            INNER JOIN regboxes rb ON rb.id = tr.id_regbox
        WHERE tr.state != 0 AND tr.type = 1
              AND DATE(rb.date_added) BETWEEN '$startDate' AND '$endDate'
        ");

            $RPT['most_selled'] = [];

            $RPT['dash']['total_margen_bruto'] = 0;
            $RPT['dash']['total_margen_igv'] = 0;
            $RPT['dash']['total_margen_service'] = 0;
            $RPT['dash']['total_margen_cost'] = 0;
            $RPT['dash']['total_margen_price'] = 0;

            $RPT['data_donut'] = [
                'cats' => [],
                'prods' => []
            ];

            $SQL->execute();
            while ($o = $SQL->fetch(PDO::FETCH_ASSOC)) {

                $key = 'pp_' . $o['pp_id'];
                if (!isset($RPT['most_selled'][$key])) {
                    $_dt = [];
                    $_dt['pr_name'] = $o['pr_name'];
                    $_dt['pp_name'] = $o['pp_name'];
                    $_dt['cost'] = 0;
                    $_dt['price'] = 0;
                    $_dt['base'] = 0;
                    $_dt['igv'] = 0;
                    $_dt['service'] = 0;
                    $_dt['num_sales'] = 0;
                    $_dt['margen_bruto'] = 0;
                    $_dt['percent'] = 0;
                    $RPT['most_selled'][$key] = $_dt;
                }

                //$cost           = 0;
                $cost = UProd::getRealCostPropre($o['pp_id'], $o['id']) * $o['quantity'];
                $price = $o['price_total'];
                $base = ($price / (100 + $o['tr_pc_igv'] + $o['tr_pc_service'])) * 100;
                $igv = ($base * $o['tr_pc_igv']) / 100;
                $service = ($base * $o['tr_pc_service']) / 100;
                $margen_bruto = $price - $cost - $igv /*- $service*/
                ;

                $om = &$RPT['most_selled'][$key];
                $om['cost'] += $cost;
                $om['price'] += $price;
                $om['base'] += $base;
                $om['igv'] += $igv;
                $om['service'] += $service;
                $om['num_sales'] += $o['quantity'];
                $om['margen_bruto'] += $margen_bruto;

                $RPT['dash']['total_margen_bruto'] += $margen_bruto;
                $RPT['dash']['total_margen_igv'] += $igv;
                $RPT['dash']['total_margen_service'] += $service;
                $RPT['dash']['total_margen_cost'] += $cost;
                $RPT['dash']['total_margen_price'] += $price;


                // CATEGORIAS
                if ($o['ca_id_parent'] > 0) {
                    $cat = $class->getParentCategoryById($o['ca_id_parent']);
                    if ($cat != null) {
                        $o['ca_id'] = $cat->id;
                        $o['ca_name'] = $cat->name;
                    }
                }

                $key_cat = 'ct_' . $o['ca_id'];
                if (!isset($RPT['data_donut']['cats'][$key_cat])) {


                    $cat = [
                        'name' => $o['ca_name'],
                        //'color' => RandomColor::getColor(),
                        'value' => 0,
                        'prods' => [],
                        'total_cost' => 0,
                        'total_price' => 0,
                        'igv' => 0,
                        'service' => 0,
                        'margen_bruto' => 0
                    ];
                    $RPT['data_donut']['cats'][$key_cat] = $cat;
                }

                $com = &$RPT['data_donut']['cats'][$key_cat];
                $com['value'] += $o['quantity'];
                $com['total_cost'] += $cost;
                $com['total_price'] += $price;
                $com['igv'] += $igv;
                $com['service'] += $service;
                $com['margen_bruto'] += $margen_bruto;

                $key_prod = 'prod_' . $o['pr_id'];

                if (!isset($com['prods'][$key_prod])) {
                    $com['prods'][$key_prod] = [
                        'cat' => $o['ca_name'],
                        'name' => $o['pr_name'],
                        'value' => 0,
                        //'color' => $com['color']
                    ];
                }

                $cat_prod = &$com['prods'][$key_prod];
                $cat_prod['value'] += $o['quantity'];
            }

            foreach ($RPT['most_selled'] as &$o) {
                if ($o['margen_bruto'] > 0) {
                    //$o['percent'] = ($o['margen_bruto']/$RPT['dash']['total_margen_bruto'])*100;
                    $o['percent'] = $o['margen_bruto'] / ($o['price'] - $o['igv']); //TODO: new
                }
            }

            // CATS
            $RPT['data_donut']['cats'] = array_values($class->array_sort($RPT['data_donut']['cats'], 'value', SORT_DESC));

            $RPT['data_donut']['prods'] = [];
            foreach ($RPT['data_donut']['cats'] as &$c) {
                if ($c['margen_bruto'] > 0) {
                    //$c['percent'] = ($c['margen_bruto']/$RPT['dash']['total_margen_bruto'])*100;
                    $c['percent'] = $c['margen_bruto'] / ($c['total_price'] - $c['igv']); //TODO: new
                } else {
                    $c['percent'] = 0;
                }

                foreach ($c['prods'] as $p) {
                    $RPT['data_donut']['prods'][] = $p;
                }
                unset($c['prods']);
                //echo $c['name'].': '.count($c['prods']);
            }
            //print_r($R['->data_donut']['prods']);
            //exit;
            echo json_encode($RPT);
            /*echo json_encode(['a'=>[
                0 => ['we'=>'ee'],

            ]]);*/
        } catch (PDOException $Err) {

            return $Err->getMessage();
        }
    }

    public function getDataInventorybyLocal($host, $db, $user, $pass)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fetchInventories = $cone->prepare("SELECT su.*,
                un.name un_name,
                t1.balance stock
                FROM kardex t1
                JOIN (SELECT MAX(id) id FROM kardex GROUP BY id_supply) t2 ON t1.id = t2.id AND t1.id = t2.id
                INNER JOIN supplies su ON su.id = t1.id_supply
                INNER JOIN unimeds un ON un.id = su.id_unimed
                WHERE su.state = 1
                GROUP BY t1.id_supply");

            $fetchInventories->execute();
            $i = 0;
            $inventoryJSON = [];
            while ($fetchInventory = $fetchInventories->fetch(PDO::FETCH_OBJ)) {
                $inventoryJSON[$i] = $fetchInventory;
                $i++;
            }
            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($inventoryJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getKardexByLocal($host, $db, $user, $pass, $startDate, $endDate, $id_supply)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fetchKardexs = $cone->prepare("
                            SELECT ka.*,
                                   su.name su_name
                            FROM kardex ka
                              JOIN supplies su ON su.id = ka.id_supply
                            WHERE ka.id_supply = $id_supply
                                  AND DATE(ka.date_added) BETWEEN '$startDate'
                                  AND '$endDate'
                        ");

            $fetchKardexs->execute();
            $i = 0;
            $kardexJSON = [];
            while ($fetchKardex = $fetchKardexs->fetch(PDO::FETCH_OBJ)) {
                $kardexJSON[$i] = $fetchKardex;
                $i++;
            }
            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($kardexJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getSupplies($host, $db, $user, $pass)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fetchSupplies = $cone->prepare("SELECT * FROM supplies WHERE state != 0 ORDER BY name");

            $fetchSupplies->execute();
            $i = 0;
            $suppliesJSON = [];
            while ($fetchSupply = $fetchSupplies->fetch(PDO::FETCH_OBJ)) {
                $suppliesJSON[$i] = $fetchSupply;
                $i++;
            }
            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($suppliesJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getAreas($host, $db, $user, $pass)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fetchAreas = $cone->prepare("SELECT * FROM areas WHERE state <> 0 ORDER BY name");

            $fetchAreas->execute();
            $i = 0;
            $areasJSON = [];
            while ($fetchArea = $fetchAreas->fetch(PDO::FETCH_OBJ)) {
                $areasJSON[$i] = $fetchArea;
                $i++;
            }
            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($areasJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getCategories($host, $db, $user, $pass)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fetchCategories = $cone->prepare("SELECT * FROM categories WHERE state = 1 ORDER BY sort");

            $fetchCategories->execute();
            $i = 0;
            $categoriesJSON = [];
            while ($fetchCategory = $fetchCategories->fetch(PDO::FETCH_OBJ)) {
                $categoriesJSON[$i] = $fetchCategory;
                $i++;
            }
            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($categoriesJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getProfs($host, $db, $user, $pass)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fetchProfs = $cone->prepare("SELECT * FROM proofs WHERE state = 1");

            $fetchProfs->execute();
            $i = 0;
            $profJSON = [];
            while ($fetchProf = $fetchProfs->fetch(PDO::FETCH_OBJ)) {
                $profJSON[$i] = $fetchProf;
                $i++;
            }
            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($profJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function mkToken($identifiers = '')
    {
        return md5(uniqid($identifiers));
    }

    public function getUser($user_email, $user_password)
    {
        $token = new ClassLocatarios();
        $getUser = $this->connectBD()->prepare("SELECT * FROM usuarios where user_email = '$user_email' and user_password = '$user_password'");
        $getUser->execute();
        $i = 0;
        $userJSON = [];
        while ($user = $getUser->fetch(PDO::FETCH_OBJ)) {
            $newToken = $token->mkToken($user->user_id);
            $user->token = $newToken;
            $userJSON['ok'] = 1;
            $userJSON['data'] = $user;
        }
        header("Access-Control-Allow-Origin:*");
        header("Content-type: application/json");
        echo json_encode($userJSON);
    }

    public function getCuentasPorCobrar($host, $db, $user, $pass)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fecthCuentas = $cone->prepare("SELECT * FROM receivables WHERE state = 1");

            $fecthCuentas->execute();
            $i = 0;
            $cuentasJSON = [];
            while ($fetchCuenta = $fecthCuentas->fetch(PDO::FETCH_OBJ)) {
                $cuentasJSON[$i] = $fetchCuenta;
                $i++;
            }
            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($cuentasJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getDataCuentasPorCobrar($host, $db, $user, $pass, $startDate, $endDate, $idCuenta)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fecthDataCuentas = $cone->prepare("SELECT tr.*,
               re.name re_name,
               pf.name pf_name
        FROM transactions tr
            JOIN regboxes rb ON rb.id = tr.id_regbox
            JOIN receivables re ON re.id = tr.id_receivable
            JOIN proofs pf ON pf.id = tr.id_proof
        WHERE
          AND rb.date_added BETWEEN '$startDate' AND '$endDate'
            AND tr.receivable_paid = 0
              AND tr.id_receivable = $idCuenta
        ORDER BY tr.id DESC");

            $fecthDataCuentas->execute();
            $i = 0;
            $cuentasDataJSON = [];
            while ($fetchDataCuenta = $fecthDataCuentas->fetch(PDO::FETCH_OBJ)) {
                $cuentasDataJSON[$i] = $fetchDataCuenta;
                $i++;
            }
            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($cuentasDataJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getCajas($host, $db, $user, $pass)
    {
        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fetchCajas = $cone->prepare("SELECT bo.id,bo.name,
               re.id re_id,
               re.id_turn re_id_turn
        FROM boxes bo
          LEFT JOIN regboxes re ON re.id_box = bo.id AND re.state = 1
        WHERE bo.id_branch = 1 AND bo.state = 1
        ORDER BY name");

            $fetchCajas->execute();
            $i = 0;
            $cajasJSON = [];
            while ($fetchCaja = $fetchCajas->fetch(PDO::FETCH_OBJ)) {
                $cajasJSON[$i] = $fetchCaja;
                $i++;
            }
            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($cajasJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getPlatosTopTime($host, $db, $user, $pass, $startDate, $endDate)
    {

        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fecthPlatos = $cone->prepare("SELECT SUM(tr.total) value,
                                SUM(tr.cash) total_cash,
                                SUM(tr.card) total_card,
                                DATE(tr.date_added) date
                                FROM transactions tr
                                INNER JOIN regboxes rb ON rb.id = tr.id_regbox
                                WHERE tr.state != 0 AND tr.type = 1
                                AND DATE(rb.date_added) BETWEEN '$startDate' AND '$endDate'
                                GROUP BY DATE(tr.date_added)");

            $fecthPlatos->execute();
            $i = 0;
            $platosJSON = [];
            while ($fecthPlato = $fecthPlatos->fetch(PDO::FETCH_OBJ)) {
                $platosJSON[$i] = $fecthPlato;
                $i++;
            }
            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($platosJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getDataTurns($host, $db, $user, $pass)
    {

        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fetchTurns = $cone->prepare("SELECT * FROM turns WHERE state = 1");
            $fetchTurns->execute();

            $i = 0;
            $turnsJSON = [];

            while ($fetchTurn = $fetchTurns->fetch(PDO::FETCH_OBJ)) {
                $turnsJSON[$i] = $fetchTurn;
                $i++;
            }

            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($turnsJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function getCierres($host, $db, $user, $pass, $id_box, $id_turn)
    {
        $WHERE = "re.state = 2";

        if ($id_box > 0) {
            $WHERE .= " AND re.id_box = $id_box";
        }

        if ($id_turn > 0) {
            $WHERE .= " AND re.id_turn = $id_turn";
        }

        try {
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fetchCierres = $cone->prepare("SELECT re.*,
                   DATE_FORMAT(re.date_added, '%d-%m-%Y %h:%i %p') date_added,
                   DATE_FORMAT(re.date_closed, '%d-%m-%Y %h:%i %p') date_closed,
                   bo.name bo_name,
                   us.name us_name
            FROM regboxes re
              INNER JOIN boxes bo ON bo.id = re.id_box
              INNER JOIN users us ON us.id = re.id_user
            WHERE $WHERE
            ORDER BY re.id DESC
            LIMIT 60
        ");
            $fetchCierres->execute();

            $i = 0;
            $cierresJSON = [];

            while ($fetchCierre = $fetchCierres->fetch(PDO::FETCH_OBJ)) {
                $cierresJSON[$i] = $fetchCierre;
                $i++;
            }

            header("Access-Control-Allow-Origin:*");
            header("Content-type: application/json");
            echo json_encode($cierresJSON);
        } catch (PDOException $Err) {
            return $Err->getMessage();
        }
    }

    public function close_box($host, $db, $user, $pass, $id_regbox, $json_items = false)
    {
        $data = [];
        $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
        $cone->query("SET NAMES 'utf8'");
        // Obtener esta caja en cierre
        $box = $cone->prepare("SELECT bo.*,
                              re.date_added re_date_added,
                              re.date_closed re_date_closed,
                              us.name us_name,
                              tu.name tu_name
                       FROM regboxes re
                        INNER JOIN boxes bo ON bo.id = re.id_box
                        INNER JOIN users us ON us.id = re.id_user
                        INNER JOIN turns tu ON tu.id = re.id_turn
                       WHERE re.id = $id_regbox");
        $box->execute();
        $box = $box->fetch(PDO::FETCH_OBJ);
        $data['user'] = $box->us_name;
        $data['turn'] = $box->tu_name;

        $data['date_open'] = date('d/m/Y h:i:s A', strtotime($box->re_date_added));
        $data['date_close'] = date('d/m/Y h:i:s A', empty($box->re_date_closed) ? time() : strtotime($box->re_date_closed));

        $data['num_pax'] = 0;

        $total_emitted = 0; // El total, incluido anulados
        $total_annul = 0;

        // No cuenta con anulados
        $total_sale = 0; // El total de emitidos menos los anulados
        $total_promo = 0;
        $total_card = 0;
        $total_cash = 0;
        $total_cash_dollar = 0;
        $total_cash_usd_pen = 0;
        $total_receivable = 0;
        $total_base = 0;
        $total_igv = 0;
        $total_service = 0;
        $total_tip = 0;
        $total_client_change = 0;

        $num_items_charged = 0;
        $num_items_annul = 0;

        $boleta_from = 0;
        $boleta_to = 0;
        $factura_from = 0;
        $factura_to = 0;

        $num_pax = 0;

        $items = [];


        // Obtener el numero de pax
        $ots = $cone->prepare("SELECT od.num_persons
                         FROM ordpros op
                          JOIN orders od ON od.id = op.id_order
                          JOIN transactions tr ON tr.id = op.id_transaction
                            JOIN proofs pr ON pr.id = tr.id_proof
                         WHERE tr.id_regbox = $id_regbox AND pr.code != '00'
                         GROUP BY op.id_order");
        $ots->execute();
        while ($ot = $ots->fetch(PDO::FETCH_OBJ)) {

            $data['num_pax'] += $ot->num_persons;
        }

        // Transacciones
        $id_transactions = [];
        $SQL = "SELECT tr.*,
                       cl.name client,
                       pr.name proof,
                       pr.code pr_code
                FROM transactions tr
                  LEFT JOIN clients cl ON cl.id = tr.id_client
                  LEFT JOIN proofs pr ON pr.id = tr.id_proof
                WHERE tr.id_regbox = $id_regbox AND pr.code != '00'
                ORDER BY tr.id";
        $os = $cone->prepare($SQL);
        $os->execute();
        while ($o = $os->fetch(PDO::FETCH_OBJ)) {
            $id_transactions[] = $o->id;

            if (empty($o->client)) {
                $o->client = 'Público en General';
            }

            $total_emitted += $o->total;

            if ($o->state == 0) {
                $num_items_annul += 1;
                $total_annul += $o->total;
            } else {
                $num_items_charged += 1;

                $total_sale += $o->total;
                $total_promo += $o->promo;
                $total_base += $o->base;
                $total_igv += $o->igv;
                $total_service += $o->service;
                $total_card += $o->card;

                if ($o->id_receivable > 0) {
                    $total_receivable += $o->cash;
                } else {
                    $total_cash += $o->cash;
                }

                $total_cash_dollar += $o->cash_dollar;
                $total_cash_usd_pen += $o->cash_usd_pen;


                $total_tip += $o->tip;
                $total_client_change += $o->client_change;
            }

            // Factura
            if ($o->pr_code == '01') {
                if ($factura_from == 0) {
                    $factura_from = $o->correlative;
                }
                $factura_to = $o->correlative;
            }
            // Boleta
            if ($o->pr_code == '03') {
                if ($boleta_from == 0) {
                    $boleta_from = $o->correlative;
                }
                $boleta_to = $o->correlative;
            }

            $o->name = strtoupper(substr($o->proof, 0, 2)) . ' ' . sprintf('%010d', $o->correlative);

            $o->value = number_format($o->total, 2, '.', '');

            $items[] = $o;
        }

        // Vamos asumir que nunca la caja va estar sin dinero en soles
        $total_cash_box = $total_cash;
        if (!empty($total_client_change)) {
            $total_cash_box = $total_cash - $total_client_change;
        }

        //$items[] = ['name' => 'NUMERO DE PAX', 'value' => $data['num_pax']];

        //TODO: obtener transaction cards para optimizar sql
        $data['cards'] = [];
        if (count($id_transactions) > 0) {
            $os = $cone->prepare("SELECT tc.*,
                                   ca.name ca_name
                            FROM transactions_cards tc
                              JOIN cards ca ON ca.id = tc.id_card
                            WHERE id_transaction IN (" . implode(',', $id_transactions) . ")");
            $os->execute();
            while ($o = $os->fetch(PDO::FETCH_OBJ)) {
                $key = $o->id_card;
                if (!isset($data['cards'][$key])) {
                    $data['cards'][$key] = [
                        'id' => $o->id_card,
                        'name' => 'Tarjeta ' . $o->ca_name,
                        '_amount' => 0,
                        'amount' => '0.00'
                    ];
                }
                $data['cards'][$key]['_amount'] += $o->amount;
                $data['cards'][$key]['amount'] = number_format($data['cards'][$key]['_amount'], 2, '.', '');
            }
        }

        $data['type'] = 'close_box';

        $data['num_items_charged'] = $num_items_charged;
        $data['num_items_annul'] = $num_items_annul;

        $data['factura_from_to'] = 'Factura del ' . sprintf('%010d', $factura_from) . ' al ' . sprintf('%010d', $factura_to);
        $data['boleta_from_to'] = 'Boleta del ' . sprintf('%010d', $boleta_from) . ' al ' . sprintf('%010d', $boleta_to);

        $data['total_emitted'] = number_format($total_emitted, 2, '.', '');
        $data['total_annul'] = number_format($total_annul, 2, '.', '');
        $data['total_promo'] = number_format($total_promo, 2, '.', '');
        $data['total_card'] = number_format($total_card, 2, '.', '');
        $data['total_cash'] = number_format($total_cash_box, 2, '.', '');
        $data['total_cash_dollar'] = number_format($total_cash_dollar, 2, '.', '');
        $data['total_cash_usd_pen'] = number_format($total_cash_usd_pen, 2, '.', '');
        $data['total_receivable'] = number_format($total_receivable, 2, '.', '');
        $data['total_base'] = number_format($total_base, 2, '.', '');
        $data['total_igv'] = number_format($total_igv, 2, '.', '');
        $data['total_service'] = number_format($total_service, 2, '.', '');
        $data['total_tip'] = number_format($total_tip, 2, '.', '');
        $data['total_sale'] = number_format($total_sale, 2, '.', '');

        $data['items'] = $json_items ? json_encode($items) : $items;

        return $data;
    }

    public function getSearchCaja($host, $db, $user, $pass, $id_regbox)
    {
        header("Access-Control-Allow-Origin:*");
        header("Content-type: application/json");
        $class = new ClassLocatarios();
        $print = $class->close_box($host, $db, $user, $pass, $id_regbox);
        $table_summary = '
        <tr>
            <th>Total Emitido</th>
            <td> S/.' . $print['total_emitted'] . ' </td>
        </tr>
        <tr>
            <th>Total Anulado</th>
            <td>  S/.' . $print['total_annul'] . ' </td>
        </tr>
        <tr>
            <th>Total Descuento</th>
            <td>  S/.' . $print['total_promo'] . ' </td>
        </tr>
        <tr>
            <th>Total con Tarjeta</th>
            <td>  S/.' . $print['total_card'] . ' </td>
        </tr>';

        foreach ($print['cards'] as $o) {
            $table_summary .= '
            <tr>
                <td>&nbsp;&nbsp;&nbsp;- ' . $o['name'] . ' </td>
                <td>  S/.' . $o['amount'] . ' </td>
            </tr>
        ';
        }

        $table_summary .= '
        <tr>
            <th>Total en Efectivo Soles</th>
            <td>  S/.' . $print['total_cash'] . ' </td>
        </tr>
        <tr>
            <th>Total en Efectivo Dolares</th>
            <td> $ ' . $print['total_cash_dollar'] . ' (S/ ' . $print['total_cash_usd_pen'] . ') </td>
        </tr>
        <tr>
            <th>Total Cuentas por cobrar</th>
            <td>' . ($print['total_receivable']) . '</td>
        </tr>
        <tr>
            <th>Total Bruto</th>
            <td>  S/.' . $print['total_base'] . ' </td>
        </tr>
        <tr>
            <th>Total IGV</th>
            <td>  S/.' . $print['total_igv'] . ' </td>
        </tr>
        <tr>
            <th>Total Servicio</th>
            <td>  S/.' . $print['total_service'] . ' </td>
        </tr>
        <tr>
            <th>Total Venta</th>
            <td>  S/.' . $print['total_sale'] . ' </td>
        </tr>
        <tr>
            <th>Total Propinas</th>
            <td>  S/.' . $print['total_tip'] . ' </td>
        </tr>
        <tr>
            <td colspan="2"> ' . $print['factura_from_to'] . ' </td>
        </tr>
        <tr>
            <td colspan="2"> ' . $print['boleta_from_to'] . ' </td>
        </tr>
        ';

        $table_sales = '';

        foreach ($print['items'] as $o) {

            if ($o->state == 0) {
                $html_state = '<span class="label label-danger">Anulado</span>';
            } else {
                $html_state = '<span class="label label-success">Activo</span>';
            }

            $table_sales .= '
                        <tr>
                            <td class="nowrap"> ' . $o->date_added . ' </td>
                            <td> ' . $o->proof . ' </td>
                            <td class="nowrap"> ' . $o->name . ' </td>
                            <td> ' . $o->client . ' </td>
                            <td class="nowrap"> ' . number_format($o->promo, 2, '.', '') . ' </td>
                            <td class="nowrap"> ' . number_format($o->tip, 2, '.', '') . ' </td>
                            <td class="nowrap"> ' . number_format($o->card, 2, '.', '') . ' </td>
                            <td class="nowrap"> S/ ' . number_format($o->cash, 2, '.', '') . ' </td>
                            <td class="nowrap"> $ ' . number_format($o->cash_dollar, 2, '.', '') . ' </td>
                            <td class="nowrap"> ' . number_format($o->total, 2, '.', '') . ' </td>
                            <td class="nowrap"> ' . $html_state . ' </td>
                        </tr>
                    ';
        }

        $print['id_regbox'] = $id_regbox;

        $data = [];
        $data['table_summary'] = $table_summary;
        $data['table_sales'] = $table_sales;
        $data['print'] = $print;
        echo json_encode($data);
    }

    public function exportSales($dataLocal, $start, $end)
    {
        $host = $dataLocal['hostname'];
        $db = $dataLocal['database'];
        $user = $dataLocal['user'];
        $pass = $dataLocal['password'];
        $reason = $dataLocal['reason'];
        $ruc = $dataLocal['ruc'];
        $startExplode = explode(" ", $start);
        $endExplode = explode(" ", $end);
        date_default_timezone_set('Europe/Madrid');
        setlocale(LC_TIME, 'spanish');

        $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        for ($i = 0; $i < count($letters); $i++) {
            $cells = "$letters[$i]1:$letters[$i]5000";
            $sheet->getColumnDimension("$letters[$i]")->setAutoSize(false);
            $sheet->getColumnDimension("$letters[$i]")->setWidth('30');
            //$sheet->getStyle($cells)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('ffffff');
        }
        $sheet->getStyle('K9:K10')->getAlignment()->setWrapText(true);
        $sheet->getStyle("C9:P9")->getAlignment()->setVertical('center');
        $sheet->getStyle("C9:P9")->getAlignment()->setHorizontal('center');
        $sheet->getStyle("C10:K10")->getAlignment()->setVertical('center');
        $sheet->getStyle("C10:K10")->getAlignment()->setHorizontal('center');

        $sheet->getStyle('C9:R9')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('bfbfbf');
        $sheet->getStyle('D10:R10')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('bfbfbf');


        $sheet->setCellValue('B2', "Reporte de Ventas - Por Día y Periodo del Día");
        $sheet->setCellValue('B3', "Precios Net (sin IGV) en Soles");
        $sheet->setCellValue('B4', "IGV");
        $sheet->setCellValue('C4', "1.18");
        $sheet->setCellValue('C6', "RAZON SOCIAL");
        $sheet->setCellValue('D6', "$reason");
        $sheet->setCellValue('C7', "RUC");
        $sheet->setCellValue('D7', "$ruc");
        $sheet->setCellValue('I7', "Periodo");
        $sheet->setCellValue('J7', "$startExplode[0] a $endExplode[0]");
        $sheet->setCellValue('C9', "Día");
        $sheet->mergeCells('C9:C10');
        $sheet->setCellValue('D9', "11H00am - 4h00pm");
        $sheet->mergeCells('D9:E9');
        $sheet->setCellValue('D10', "N°");
        $sheet->setCellValue('E10', "S/.");
        $sheet->setCellValue('F9', "4h00pm - 9h00pm");
        $sheet->mergeCells('F9:G9');
        $sheet->setCellValue('F10', "N°");
        $sheet->setCellValue('G10', "S/.");
        $sheet->setCellValue('H9', "9h00pm - 11h59pm");
        $sheet->mergeCells('H9:I9');
        $sheet->setCellValue('H10', "N°");
        $sheet->setCellValue('I10', "S/.");
        $sheet->setCellValue('J9', "Total");
        $sheet->mergeCells('J9:N9');
        $sheet->setCellValue('J10', "N°");
        $sheet->setCellValue('K10', "Total (S/.)");
        $sheet->setCellValue('L10', "Subtotal (S/.)");
        $sheet->setCellValue('M10', "IGV (S/.)");
        $sheet->setCellValue('N10', "Servicios (S/.)");
        $sheet->setCellValue('O9', "Variable S/");
        $sheet->setCellValue('O10', "12%");
        $sheet->setCellValue('P9', "Tipo de Cambio(venta Sunat)");
        $sheet->mergeCells('P9:P10');
        $sheet->setCellValue('Q9', "Total");
        $sheet->setCellValue('Q10', "USD");
        $sheet->setCellValue('R9', "Variable");
        $sheet->setCellValue('R10', "USD");

        $index = 11;
        $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
        $cone->query("SET NAMES 'utf8'");
        $fetchTransactions = $cone->prepare("SELECT 
                 tr.*,
               cl.name cl_name,
               pf.name pf_name
        FROM transactions tr
          LEFT JOIN clients cl ON cl.id = tr.id_client
          LEFT JOIN proofs pf ON pf.id = tr.id_proof
        WHERE tr.id_branch = 1
              AND tr.date_added BETWEEN '$start' AND '$end' AND tr.state <> 0 
              AND (pf.id = 2 OR pf.id = 5)
        ORDER BY tr.date_added ASC");
        $fetchTransactions->execute();

        $dateStartCompareBlock1 = "10:00:00";
        $dateEndCompareBlock1 = "16:00:00";
        $dateStartCompareBlock2 = "16:00:00";
        $dateEndCompareBlock2 = "21:00:00";
        $dateStartCompareBlock3 = "21:00:00";
        $dateEndCompareBlock3 = "23:59:59";
        $sales = [];
        $total_quantity_block_1 = 0;
        $total_sales_block_1 = 0;
        $total_quantity_block_2 = 0;
        $total_sales_block_2 = 0;
        $total_quantity_block_3 = 0;
        $total_sales_block_3 = 0;
        $total_quantity_all = 0;
        $total_sales_all_soles = 0;
        $total_bases_all_soles = 0;
        $total_igv_all_soles = 0;
        $total_services_all_soles = 0;
        $total_variables_soles = 0;
        $total_sales_all_dollars = 0;
        $total_variables_dollars = 0;

        while ($fetchTransaction = $fetchTransactions->fetch(PDO::FETCH_OBJ)) {
            $indexDate = explode(" ", $fetchTransaction->date_added);
            if (!isset($sales[$indexDate[0]])) {
                $sales[$indexDate[0]] =
                    [
                        "date" => "",
                        "quantity_block_1" => 0,
                        "total_block_1" => 0,
                        "quantity_block_2" => 0,
                        "total_block_2" => 0,
                        "quantity_block_3" => 0,
                        "total_block_3" => 0,
                        "total" => 0,
                        "base" => 0,
                        "igv" => 0,
                        "service" => 0,
                        "tc" => 0,
                        "quantity_total" => 0,
                        "date_1" => "",
                        "date_2" => "",
                        "date_3" => ""
                    ];
            }
            if ($fetchTransaction->date_added >= ($indexDate[0] . " " . $dateStartCompareBlock1) && $fetchTransaction->date_added <= ($indexDate[0] . " " . $dateEndCompareBlock1)) {
                $sales[$indexDate[0]]['quantity_block_1'] += 1;
                $sales[$indexDate[0]]['total_block_1'] += $fetchTransaction->base;
                $sales[$indexDate[0]]['date_1'] .= $fetchTransaction->date_added . "/";
            } elseif ($fetchTransaction->date_added > ($indexDate[0] . " " . $dateStartCompareBlock2) && $fetchTransaction->date_added <= ($indexDate[0] . " " . $dateEndCompareBlock2)) {
                $sales[$indexDate[0]]['quantity_block_2'] += 1;
                $sales[$indexDate[0]]['total_block_2'] += $fetchTransaction->base;
                $sales[$indexDate[0]]['date_2'] .= $fetchTransaction->date_added . "/";
            }elseif ($fetchTransaction->date_added > ($indexDate[0] . " " . $dateStartCompareBlock3) && $fetchTransaction->date_added <= ($indexDate[0] . " " . $dateEndCompareBlock3)) {
                $sales[$indexDate[0]]['quantity_block_3'] += 1;
                $sales[$indexDate[0]]['total_block_3'] += $fetchTransaction->base;
                $sales[$indexDate[0]]['date_3'] .= $fetchTransaction->date_added . "/";
            }

            $sales[$indexDate[0]]['date'] = $indexDate[0];
            $sales[$indexDate[0]]['total'] += $fetchTransaction->total;
            $sales[$indexDate[0]]['base'] += $fetchTransaction->base;
            $sales[$indexDate[0]]['igv'] += $fetchTransaction->igv;
            $sales[$indexDate[0]]['service'] += $fetchTransaction->service;
            $sales[$indexDate[0]]['tc'] = $fetchTransaction->tc;
            $sales[$indexDate[0]]['quantity_total'] += 1;
            /*if ($fetchTransaction->date_added >= $dateStartCompareBlock1) {
                $i++;
            } else if($fetchTransaction->date_added >= $dateStartCompareBlock1){

            }*/
        }
        foreach ($sales as $sale) {

            $total_quantity_block_1 += $sale['quantity_block_1'];
            $total_sales_block_1 += $sale['total_block_1'];
            $total_quantity_block_2 += $sale['quantity_block_2'];
            $total_sales_block_2 += $sale['total_block_2'];
            $total_quantity_block_3 += $sale['quantity_block_3'];
            $total_sales_block_3 += $sale['total_block_3'];
            $total_quantity_all += $sale['quantity_total'];
            $total_sales_all_soles += $sale['total'];
            $total_bases_all_soles += $sale['base'];
            $total_igv_all_soles += $sale['igv'];
            $total_services_all_soles += $sale['service'];
            $total_variables_soles += (($sale['base']) * 0.12);
            $total_sales_all_dollars += (($sale['base']) / $sale['tc']);
            $total_variables_dollars += ((($sale['base']) / $sale['tc']) * 0.12);

            $sheet->setCellValue('C' . $index, utf8_encode(strftime("%A, %d de %B del %Y", strtotime($sale['date']))));
            $sheet->setCellValue('D' . $index, $sale['quantity_block_1']);
            $sheet->setCellValue('E' . $index, $sale['total_block_1']);
            $sheet->setCellValue('F' . $index, $sale['quantity_block_2']);
            $sheet->setCellValue('G' . $index, $sale['total_block_2']);
            $sheet->setCellValue('H' . $index, $sale['quantity_block_3']);
            $sheet->setCellValue('I' . $index, $sale['total_block_3']);
            $sheet->setCellValue('J' . $index, $sale['quantity_total']);
            $sheet->setCellValue('K' . $index, $sale['total']);
            $sheet->setCellValue('L' . $index, $sale['base']);
            $sheet->setCellValue('M' . $index, $sale['igv']);
            $sheet->setCellValue('N' . $index, $sale['service']);
            $sheet->setCellValue('O' . $index, (($sale['base']) * 0.12));
            $sheet->setCellValue('P' . $index, $sale['tc']);
            $sheet->setCellValue('Q' . $index, (($sale['base']) / $sale['tc']));
            $sheet->setCellValue('R' . $index, ((($sale['base']) / $sale['tc']) * 0.12));
            $index++;
        }

        $sheet->setCellValue('C' . ($index), 'TOTAL');
        $sheet->setCellValue('D' . ($index), $total_quantity_block_1);
        $sheet->setCellValue('E' . ($index), $total_sales_block_1);
        $sheet->setCellValue('F' . ($index), $total_quantity_block_2);
        $sheet->setCellValue('G' . ($index), $total_sales_block_2);
        $sheet->setCellValue('J' . ($index), $total_quantity_all);
        $sheet->setCellValue('K' . ($index), $total_sales_all_soles);
        $sheet->setCellValue('L' . ($index), $total_bases_all_soles);
        $sheet->setCellValue('M' . ($index), $total_igv_all_soles);
        $sheet->setCellValue('N' . ($index), $total_services_all_soles);
        $sheet->setCellValue('O' . ($index), $total_variables_soles);
        $sheet->setCellValue('Q' . ($index), $total_sales_all_dollars);
        $sheet->setCellValue('R' . ($index), $total_variables_dollars);

        #color plomo a la fila de totales
        $sheet->getStyle("C$index:R$index")->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('bfbfbf');

        $sheet->getStyle("H11:H" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("I11:I" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("J11:J" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("K11:K" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("L9:L" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("M9:M" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("N9:N" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("O9:O" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("P9:P" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("Q9:Q" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("R9:R" . ($index + 1))->getAlignment()->setHorizontal('center');

        $sheet->getStyle("D11:D" . ($index + 1))->getAlignment()->setVertical('center');
        $sheet->getStyle("D11:D" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("E11:E" . ($index + 1))->getAlignment()->setVertical('center');
        $sheet->getStyle("E11:E" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("F11:F" . ($index + 1))->getAlignment()->setVertical('center');
        $sheet->getStyle("F11:F" . ($index + 1))->getAlignment()->setHorizontal('center');
        $sheet->getStyle("G11:G" . ($index + 1))->getAlignment()->setVertical('center');
        $sheet->getStyle("G11:G" . ($index + 1))->getAlignment()->setHorizontal('center');
        //$sheet->getStyle('H11:H5000')->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('E11:E' . ($index + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        //$sheet->getStyle('F11:F5000')->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('I11:I' . ($index + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        //$sheet->getStyle('J11:J' . ($index + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('K11:K' . ($index + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('L11:L' . ($index + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('M11:M' . ($index + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('N11:N' . ($index + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('O11:O' . ($index + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('P11:P' . ($index + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('Q11:Q' . ($index + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('R11:R' . ($index + 1))->getNumberFormat()->setFormatCode('#,##0.00');


        /*$fetchTransactions = $cone->prepare("
                SELECT COUNT(*) AS cantidadByDay,
                SUM(tr.total) AS totalByDay,
					SUM(tr.base) AS baseByDay,
					SUM(tr.igv) AS igvByDay,
					SUM(tr.service) AS serviceByDay,
                 tr.*,
               cl.name cl_name,
               pf.name pf_name
        FROM transactions tr
          LEFT JOIN clients cl ON cl.id = tr.id_client
          LEFT JOIN proofs pf ON pf.id = tr.id_proof
        WHERE tr.id_branch = 1
              AND tr.date_added BETWEEN '$start' AND '$end' AND tr.state <> 0
              AND (pf.id = 2 OR pf.id = 5)
        GROUP BY DATE(tr.date_added) ORDER BY tr.date_added ASC");

        $fetchTransactions->execute();
        while ($fetchTransaction = $fetchTransactions->fetch(PDO::FETCH_OBJ)) {

            //$sheet->setCellValue('C' . $index, date("l, d-m-Y ", strtotime($fetchTransaction->date_added)));
            $sheet->setCellValue('C' . $index, utf8_encode(strftime("%A, %d de %B del %Y", strtotime($fetchTransaction->date_added))));
            $sheet->setCellValue('H' . $index, $fetchTransaction->cantidadByDay);
            $sheet->setCellValue('I' . $index, ($fetchTransaction->totalByDay));
            $sheet->setCellValue('J' . $index, ($fetchTransaction->baseByDay));
            $sheet->setCellValue('K' . $index, ($fetchTransaction->igvByDay));
            $sheet->setCellValue('L' . $index, ($fetchTransaction->serviceByDay));
            $sheet->setCellValue('M' . $index, (($fetchTransaction->baseByDay) * 0.12));
            $sheet->setCellValue('N' . $index, $fetchTransaction->tc);
            $sheet->setCellValue('O' . $index, (($fetchTransaction->baseByDay) / $fetchTransaction->tc));
            $sheet->setCellValue('P' . $index, ((($fetchTransaction->baseByDay) / $fetchTransaction->tc) * 0.12));
            $index++;
        }*/

        header("Access-Control-Allow-Origin:*");
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="RVENTAS_' . $startExplode[0] . '_AL_' . $endExplode[0] . '.xlsx"');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit();
    }

    public function exportAllSales($dataLocal, $start, $end)
    {
        date_default_timezone_set('Europe/Madrid');
        setlocale(LC_TIME, 'spanish');
        $startExplode = explode(" ", $start);
        $endExplode = explode(" ", $end);

        $spreadsheet = new Spreadsheet();
        #REPORTE TOTAL
        $objWorkSheet = $spreadsheet->createSheet(0);
        $objWorkSheet->getColumnDimension("B")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("B")->setWidth('30');
        $objWorkSheet->getColumnDimension("C")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("C")->setWidth('30');

        $objWorkSheet->setCellValue('B2', "REPORTE TOTAL DE VENTAS DEL " . $startExplode[0] . " al " . $endExplode[0]);
        $objWorkSheet->mergeCells('B2:C2');
        $objWorkSheet->setCellValue('B3', "* No incluyen cortesías ni anulaciones");
        $objWorkSheet->mergeCells('B3:C3');
        $objWorkSheet->getStyle('B2:C2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $objWorkSheet->getStyle('B2')->getFont()->setBold(false)->getColor()->setARGB("ffffff");
        $objWorkSheet->getStyle("B2:C2")->getAlignment()->setHorizontal('center');
        $objWorkSheet->getStyle("B3:C3")->getAlignment()->setHorizontal('left');

        #REPORTE TOTAL RESUMEN
        $objWorkSheet->setCellValue('G2', "Resúmen");
        $objWorkSheet->mergeCells('G2:Q2');
        $objWorkSheet->getStyle('G2:Q2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $objWorkSheet->getStyle('G2')->getFont()->setBold(false)->getColor()->setARGB("ffffff");
        $objWorkSheet->getStyle('G3:Q3')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $objWorkSheet->getStyle('G3:Q3')->getFont()->setBold(false)->getColor()->setARGB("ffffff");
        $objWorkSheet->getStyle("G2:Q2")->getAlignment()->setHorizontal('center');
        $objWorkSheet->getStyle("G3:Q3")->getAlignment()->setHorizontal('center');
        $objWorkSheet->getColumnDimension("G")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("G")->setWidth('20');
        $objWorkSheet->getColumnDimension("H")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("H")->setWidth('40');
        $objWorkSheet->getColumnDimension("I")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("I")->setWidth('40');
        $objWorkSheet->getColumnDimension("J")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("J")->setWidth('40');
        $objWorkSheet->getColumnDimension("K")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("K")->setWidth('40');
        $objWorkSheet->getColumnDimension("L")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("L")->setWidth('40');
        $objWorkSheet->getColumnDimension("M")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("M")->setWidth('40');
        $objWorkSheet->getColumnDimension("N")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("N")->setWidth('20');
        $objWorkSheet->getColumnDimension("O")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("O")->setWidth('20');
        $objWorkSheet->getColumnDimension("P")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("P")->setWidth('20');
        $objWorkSheet->getColumnDimension("Q")->setAutoSize(false);
        $objWorkSheet->getColumnDimension("Q")->setWidth('20');
        $objWorkSheet->setCellValue('G3', "LOCAL");
        $objWorkSheet->setCellValue('H3', "TOTAL TARJETA (S/.)");
        $objWorkSheet->setCellValue('I3', "TOTAL EFECTIVO (S/.)");
        $objWorkSheet->setCellValue('J3', "TOTAL DE VENTAS EN SALON (S/.)");
        $objWorkSheet->setCellValue('K3', "TOTAL DE VENTAS PARA LLEVAR (S/.)");
        $objWorkSheet->setCellValue('L3', "TOTAL DE VENTAS DELIVERY (S/.)");
        $objWorkSheet->setCellValue('M3', "TOTAL DE VENTAS SIN DEFINIR (S/.)");
        $objWorkSheet->setCellValue('N3', "SUBTOTAL (S/.)");
        $objWorkSheet->setCellValue('O3', "TOTAL IGV (S/.)");
        $objWorkSheet->setCellValue('P3', "TOTAL SERVICIOS (S/.)");
        $objWorkSheet->setCellValue('Q3', "TOTAL (S/.)");

        $indexStart = 4;
        $indexStartSummary = 4;
        $totalSalesCardsAllLocal = 0;
        $totalSalesCashAllLocal = 0;
        $totalSalesSalonAllLocal = 0;
        $totalSalesOutAllLocal = 0;
        $totalSalesDeliveryAllLocal = 0;
        $totalSalesNoDefineAllLocal = 0;
        $totalSalesSubAllLocal = 0;
        $totalSalesIGVAllLocal = 0;
        $totalSalesServicesAllLocal = 0;
        $totalSalesAllLocal = 0;
        for ($i = 1; $i <= count($dataLocal); $i++) {
            $totalCardByLocal = 0;
            $totalCashByLocal = 0;
            $totalSalonByLocal = 0;
            $totalOutByLocal = 0;
            $totalDeliveryByLocal = 0;
            $totalNoDefinedByLocal = 0;
            $totalBaseByLocal = 0;
            $totalIGVByLocal = 0;
            $totalServiceByLocal = 0;
            $totalSalesByLocal = 0;
            $indexTotal = $indexStart;
            $objWorkSheet->setCellValue('B' . $indexStart, "" . $dataLocal[$i - 1]['name'] . " (S/.)");
            $objWorkSheet->getStyle('B' . $indexStart)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $objWorkSheet->getStyle('B' . $indexStart)->getFont()->setBold(false)->getColor()->setARGB("ffffff");
            $host = $dataLocal[$i - 1]['hostname'];
            $db = $dataLocal[$i - 1]['database'];
            $user = $dataLocal[$i - 1]['user'];
            $pass = $dataLocal[$i - 1]['password'];
            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");
            $fetchSales = $cone->prepare("
                SELECT  COUNT(*) AS cantidadByDay,
                        SUM(tr.total) AS totalByDay,
                        SUM(tr.base) AS baseByDay,
                        SUM(tr.igv) AS igvByDay,
                        SUM(tr.service) AS serviceByDay,
                        SUM(if(tr.type_sale = 1, tr.total, 0)) AS salonByDay,
                        SUM(if(tr.type_sale = 2, tr.total, 0)) AS llevarByDay,
                        SUM(if(tr.type_sale = 3, tr.total, 0)) AS deliveryByDay,
                        SUM(if(tr.type_sale = 0, tr.total, 0)) AS notDefineByDay,
                        SUM(tr.card) AS cardsByDay,
                        SUM(tr.cash) AS cashByDay,
                        tr.date_added,
                        cl.name cl_name,
                        pf.name pf_name
                FROM transactions tr
                LEFT JOIN clients cl ON cl.id = tr.id_client
                LEFT JOIN proofs pf ON pf.id = tr.id_proof
                WHERE tr.id_branch = 1
                    AND tr.date_added BETWEEN '$start' AND '$end' AND tr.state <> 0 
                AND (pf.id = 2 OR pf.id = 5)
                GROUP BY DATE(tr.date_added) ORDER BY tr.date_added ASC");

            $fetchSales->execute();

            while ($fetchTransaction = $fetchSales->fetch(PDO::FETCH_OBJ)) {
                $totalCardByLocal += (float)$fetchTransaction->cardsByDay;
                $totalCashByLocal += (float)$fetchTransaction->cashByDay;
                $totalSalonByLocal += (float)$fetchTransaction->salonByDay;
                $totalOutByLocal += (float)$fetchTransaction->llevarByDay;
                $totalDeliveryByLocal += (float)$fetchTransaction->deliveryByDay;
                $totalNoDefinedByLocal += (float)$fetchTransaction->notDefineByDay;
                $totalBaseByLocal += (float)$fetchTransaction->baseByDay;
                $totalIGVByLocal += (float)$fetchTransaction->igvByDay;
                $totalServiceByLocal += (float)$fetchTransaction->serviceByDay;
                $totalSalesByLocal += (float)$fetchTransaction->totalByDay;

                //$sheet->setCellValue('C' . $index, date("l, d-m-Y ", strtotime($fetchTransaction->date_added)));
                $objWorkSheet->setCellValue('B' . ($indexStart + 1), utf8_encode(strftime("%A, %d de %B del %Y", strtotime($fetchTransaction->date_added))));
                $objWorkSheet->setCellValue('C' . ($indexStart + 1), ($fetchTransaction->baseByDay));
                $indexStart++;
            }
            $objWorkSheet->getStyle('C' . $indexTotal)->getAlignment()->setHorizontal('right');
            $objWorkSheet->setCellValue('C' . $indexTotal, ($totalBaseByLocal));

            $totalSalesCardsAllLocal += $totalCardByLocal;
            $totalSalesCashAllLocal += $totalCashByLocal;
            $totalSalesSalonAllLocal += $totalSalonByLocal;
            $totalSalesOutAllLocal += $totalOutByLocal;
            $totalSalesDeliveryAllLocal += $totalDeliveryByLocal;
            $totalSalesNoDefineAllLocal += $totalNoDefinedByLocal;
            $totalSalesSubAllLocal += $totalBaseByLocal;
            $totalSalesIGVAllLocal += $totalIGVByLocal;
            $totalSalesServicesAllLocal += $totalServiceByLocal;
            $totalSalesAllLocal += $totalSalesByLocal;
            #SUMMARY
            $objWorkSheet->setCellValue('G' . $indexStartSummary, "" . $dataLocal[$i - 1]['name']);
            $objWorkSheet->setCellValue('H' . $indexStartSummary, $totalCardByLocal);
            $objWorkSheet->setCellValue('I' . $indexStartSummary, $totalCashByLocal);
            $objWorkSheet->setCellValue('J' . $indexStartSummary, $totalSalonByLocal);
            $objWorkSheet->setCellValue('K' . $indexStartSummary, $totalOutByLocal);
            $objWorkSheet->setCellValue('L' . $indexStartSummary, $totalDeliveryByLocal);
            $objWorkSheet->setCellValue('M' . $indexStartSummary, $totalNoDefinedByLocal);
            $objWorkSheet->setCellValue('N' . $indexStartSummary, $totalBaseByLocal);
            $objWorkSheet->setCellValue('O' . $indexStartSummary, $totalIGVByLocal);
            $objWorkSheet->setCellValue('P' . $indexStartSummary, $totalServiceByLocal);
            $objWorkSheet->setCellValue('Q' . $indexStartSummary, $totalSalesByLocal);
            $indexStart = $indexStart + 2;
            $indexStartSummary++;
        }

        $objWorkSheet->getStyle("C1:C$indexStart")->getNumberFormat()->setFormatCode('#,##0.00');
        $objWorkSheet->getStyle("H1:H" . ($indexStartSummary + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $objWorkSheet->getStyle("I1:I" . ($indexStartSummary + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $objWorkSheet->getStyle("J1:J" . ($indexStartSummary + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $objWorkSheet->getStyle("K1:K" . ($indexStartSummary + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $objWorkSheet->getStyle("K1:L" . ($indexStartSummary + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $objWorkSheet->getStyle("K1:M" . ($indexStartSummary + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $objWorkSheet->getStyle("K1:N" . ($indexStartSummary + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $objWorkSheet->getStyle("K1:O" . ($indexStartSummary + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $objWorkSheet->getStyle("K1:P" . ($indexStartSummary + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $objWorkSheet->getStyle("K1:Q" . ($indexStartSummary + 1))->getNumberFormat()->setFormatCode('#,##0.00');
        $objWorkSheet->setCellValue('G' . ($indexStartSummary + 1), 'Total (S/.)');
        $objWorkSheet->setCellValue('H' . ($indexStartSummary + 1), $totalSalesCardsAllLocal);
        $objWorkSheet->setCellValue('I' . ($indexStartSummary + 1), $totalSalesCashAllLocal);
        $objWorkSheet->setCellValue('J' . ($indexStartSummary + 1), $totalSalesSalonAllLocal);
        $objWorkSheet->setCellValue('K' . ($indexStartSummary + 1), $totalSalesOutAllLocal);
        $objWorkSheet->setCellValue('L' . ($indexStartSummary + 1), $totalSalesDeliveryAllLocal);
        $objWorkSheet->setCellValue('M' . ($indexStartSummary + 1), $totalSalesNoDefineAllLocal);
        $objWorkSheet->setCellValue('N' . ($indexStartSummary + 1), $totalSalesSubAllLocal);
        $objWorkSheet->setCellValue('O' . ($indexStartSummary + 1), $totalSalesIGVAllLocal);
        $objWorkSheet->setCellValue('P' . ($indexStartSummary + 1), $totalSalesServicesAllLocal);
        $objWorkSheet->setCellValue('Q' . ($indexStartSummary + 1), $totalSalesAllLocal);
        $objWorkSheet->setTitle("REPORTE TOTAL");
        //LIBROS
        for ($i = 1; $i <= count($dataLocal); $i++) {
            $host = $dataLocal[$i - 1]['hostname'];
            $db = $dataLocal[$i - 1]['database'];
            $user = $dataLocal[$i - 1]['user'];
            $pass = $dataLocal[$i - 1]['password'];
            $objWorkSheet = $spreadsheet->createSheet($i);

            #CABECERA DE LA TABLA DE VENTAS DE UN LOCATARIO(LIBRO)
            $objWorkSheet->getColumnDimension("C")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("C")->setWidth('30');
            $objWorkSheet->getColumnDimension("D")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("D")->setWidth('30');
            $objWorkSheet->getColumnDimension("E")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("E")->setWidth('30');
            $objWorkSheet->getColumnDimension("F")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("F")->setWidth('30');
            $objWorkSheet->getColumnDimension("G")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("G")->setWidth('30');
            $objWorkSheet->getStyle('B2:M2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $objWorkSheet->getStyle('B2:M2')->getFont()->setBold(false)->getColor()->setARGB("ffffff");
            $objWorkSheet->setCellValue('B1', 'REGISTROS DE VENTAS');
            $objWorkSheet->mergeCells('B1:M1');
            $objWorkSheet->getStyle("B1:M1")->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle('B1:M1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00b0f0');
            $objWorkSheet->getStyle('B1')->getFont()->setBold(true)->getColor()->setARGB("000000");


            #CABECERA DE LA TABLA DE VENTAS ANULADAS DE UN LOCATARIO(LIBRO)
            $objWorkSheet->getColumnDimension("Q")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("Q")->setWidth('30');
            $objWorkSheet->getColumnDimension("R")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("R")->setWidth('30');
            $objWorkSheet->getColumnDimension("S")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("S")->setWidth('20');
            $objWorkSheet->getColumnDimension("T")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("T")->setWidth('20');
            $objWorkSheet->getColumnDimension("U")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("U")->setWidth('20');
            $objWorkSheet->getColumnDimension("AB")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("AB")->setWidth('50');

            $objWorkSheet->getStyle('P2:AB2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $objWorkSheet->getStyle('P2:AB2')->getFont()->setBold(false)->getColor()->setARGB("ffffff");
            $objWorkSheet->setCellValue('P1', 'REGISTROS DE ANULACIONES');
            $objWorkSheet->mergeCells('P1:AB1');
            $objWorkSheet->getStyle("P1:AB1")->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle('P1:AB1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00b0f0');
            $objWorkSheet->getStyle('P1')->getFont()->setBold(true)->getColor()->setARGB("000000");

            #CABECERA DE OTRAS TRANSACCIONES(CORTESIAS, ETC)
            $objWorkSheet->getColumnDimension("AD")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("AD")->setWidth('30');
            $objWorkSheet->getColumnDimension("AE")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("AE")->setWidth('30');
            $objWorkSheet->getColumnDimension("AF")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("AF")->setWidth('30');
            $objWorkSheet->getColumnDimension("AG")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("AG")->setWidth('30');
            $objWorkSheet->getColumnDimension("AH")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("AH")->setWidth('30');
            $objWorkSheet->getColumnDimension("AI")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("AI")->setWidth('30');
            $objWorkSheet->getColumnDimension("AJ")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("AJ")->setWidth('30');
            $objWorkSheet->getColumnDimension("AK")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("AK")->setWidth('30');
            $objWorkSheet->getColumnDimension("AL")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("AL")->setWidth('30');
            $objWorkSheet->getColumnDimension("AM")->setAutoSize(false);
            $objWorkSheet->getColumnDimension("AM")->setWidth('30');

            $objWorkSheet->getStyle('AE2:AO2')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
            $objWorkSheet->getStyle('AE2:AO2')->getFont()->setBold(false)->getColor()->setARGB("ffffff");
            $objWorkSheet->setCellValue('AE1', 'REGISTROS DE OTRAS TRANSACCIONES(CORTESIAS)');
            $objWorkSheet->mergeCells('AE1:AO1');
            $objWorkSheet->getStyle("AE1:AO1")->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle('AE1:AO1')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('00b0f0');
            $objWorkSheet->getStyle('AE1')->getFont()->setBold(true)->getColor()->setARGB("000000");

            $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
            $cone->query("SET NAMES 'utf8'");

            /*$fetchSales = $cone->prepare("
                SELECT  tr.id, tr.date_added, cl.name cl_name, tr.total, 
                        pf.name pf_name, tr.serie, tr.correlative
                FROM transactions tr
                LEFT JOIN clients cl ON cl.id = tr.id_client
                LEFT JOIN proofs pf ON pf.id = tr.id_proof
                WHERE tr.id_branch = 1
                    AND tr.date_added BETWEEN '$start' AND '$end' AND tr.state <> 0 AND (pf.id = 2 OR pf.id = 5)
                ORDER BY tr.id ASC");*/

            $fetchSales = $cone->prepare("
                SELECT  tr.id, tr.date_added, cl.name cl_name, tr.total, 
                        pf.name pf_name, tr.serie, tr.correlative, pf.id prof, tr.state, tr.annul_reason,
                        tr.base,tr.igv,tr.service, tr.with_card,tr.card,tr.cash
                FROM transactions tr
                LEFT JOIN clients cl ON cl.id = tr.id_client
                LEFT JOIN proofs pf ON pf.id = tr.id_proof
                WHERE tr.id_branch = 1
                    AND tr.date_added BETWEEN '$start' AND '$end'
                ORDER BY tr.date_added ASC");
            $fetchSales->execute();
            $objWorkSheet->setCellValue('B2', '#')
                ->setCellValue('C2', 'Fecha de Registro')
                ->setCellValue('D2', 'Cliente')
                ->setCellValue('E2', 'Base')
                ->setCellValue('F2', 'Total IGV')
                ->setCellValue('G2', 'Total Servicios')
                ->setCellValue('H2', 'Tarjeta')
                ->setCellValue('I2', 'Efectivo')
                ->setCellValue('J2', 'Total')
                ->setCellValue('K2', 'Doc.')
                ->setCellValue('L2', 'Serie')
                ->setCellValue('M2', 'Nro. Doc');

            $objWorkSheet->setCellValue('P2', '#')
                ->setCellValue('Q2', 'Fecha de Registro')
                ->setCellValue('R2', 'Cliente')
                ->setCellValue('S2', 'Base')
                ->setCellValue('T2', 'Total IGV')
                ->setCellValue('U2', 'Total Servicios')
                ->setCellValue('V2', 'Tarjeta')
                ->setCellValue('W2', 'Efectivo')
                ->setCellValue('X2', 'Total')
                ->setCellValue('Y2', 'DOC.')
                ->setCellValue('Z2', 'Serie')
                ->setCellValue('AA2', 'Nro. Doc')
                ->setCellValue('AB2', 'Motivo de Anulación');

            $objWorkSheet->setCellValue('AE2', '#')
                ->setCellValue('AF2', 'Fecha de Registro')
                ->setCellValue('AG2', 'Cliente')
                ->setCellValue('AH2', 'Base')
                ->setCellValue('AI2', 'Total IGV')
                ->setCellValue('AJ2', 'Total Servicios')
                ->setCellValue('AK2', 'Tarjeta')
                ->setCellValue('AL2', 'Efectivo')
                ->setCellValue('AM2', 'Total')
                ->setCellValue('AN2', 'DOC.')
                ->setCellValue('AO2', 'Nro. Doc');
            $indexStartLibsSales = 3;
            $k = 1;
            $salesTemp = array();
            $totalCellOk = 0;
            #VENTAS ACTIVAS(BOLETAS Y FACTURAS)
            while ($fetchTransaction = $fetchSales->fetch(PDO::FETCH_OBJ)) {
                array_push($salesTemp, $fetchTransaction);
                if ($fetchTransaction->state == 1) {
                    if (in_array($fetchTransaction->prof, [2, 5])) {
                        $totalCellOk += $fetchTransaction->total;
                        $objWorkSheet->setCellValue('B' . $indexStartLibsSales, $k)
                            ->setCellValue('C' . $indexStartLibsSales, $fetchTransaction->date_added)
                            ->setCellValue('D' . $indexStartLibsSales, $fetchTransaction->cl_name)
                            ->setCellValue('E' . $indexStartLibsSales, ($fetchTransaction->base))
                            ->setCellValue('F' . $indexStartLibsSales, ($fetchTransaction->igv))
                            ->setCellValue('G' . $indexStartLibsSales, ($fetchTransaction->service))
                            ->setCellValue('H' . $indexStartLibsSales, $fetchTransaction->card)
                            ->setCellValue('I' . $indexStartLibsSales, $fetchTransaction->cash)
                            ->setCellValue('J' . $indexStartLibsSales, $fetchTransaction->total)
                            ->setCellValue('K' . $indexStartLibsSales, $fetchTransaction->pf_name)
                            ->setCellValue('L' . $indexStartLibsSales, $fetchTransaction->serie)
                            ->setCellValue('M' . $indexStartLibsSales, $fetchTransaction->correlative);
                        $indexStartLibsSales++;
                        $k++;
                    }
                }
            }

            $objWorkSheet->setCellValue('I' . ($indexStartLibsSales + 1), 'Total');
            $objWorkSheet->setCellValue('J' . ($indexStartLibsSales + 1), $totalCellOk);

            $objWorkSheet->getStyle("B2:B" . ($indexStartLibsSales + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("E2:E" . ($indexStartLibsSales + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("F2:F" . ($indexStartLibsSales + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("G2:G" . ($indexStartLibsSales + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("H2:H" . ($indexStartLibsSales + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("K2:K" . ($indexStartLibsSales + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("E1:E" . ($indexStartLibsSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle("F1:F" . ($indexStartLibsSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle("G1:G" . ($indexStartLibsSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle("H1:H" . ($indexStartLibsSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle("I1:I" . ($indexStartLibsSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle("J1:J" . ($indexStartLibsSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');


            #VENTAS ANULADAS(BOLETAS Y FACTURAS)
            $indexStartLibsCancellations = 3;
            $j = 1;
            $totalCellAnnul = 0;
            foreach ($salesTemp as $fetchTransactionCancellation) {
                if ($fetchTransactionCancellation->state == 0) {
                    if (in_array($fetchTransactionCancellation->prof, [2, 5])) {
                        $totalCellAnnul += $fetchTransactionCancellation->total;
                        $objWorkSheet->setCellValue('P' . $indexStartLibsCancellations, $j)
                            ->setCellValue('Q' . $indexStartLibsCancellations, $fetchTransactionCancellation->date_added)
                            ->setCellValue('R' . $indexStartLibsCancellations, $fetchTransactionCancellation->cl_name)
                            ->setCellValue('S' . $indexStartLibsCancellations, $fetchTransactionCancellation->base)
                            ->setCellValue('T' . $indexStartLibsCancellations, $fetchTransactionCancellation->igv)
                            ->setCellValue('U' . $indexStartLibsCancellations, $fetchTransactionCancellation->service)
                            ->setCellValue('V' . $indexStartLibsCancellations, $fetchTransactionCancellation->card)
                            ->setCellValue('W' . $indexStartLibsCancellations, $fetchTransactionCancellation->cash)
                            ->setCellValue('X' . $indexStartLibsCancellations, $fetchTransactionCancellation->total)
                            ->setCellValue('Y' . $indexStartLibsCancellations, $fetchTransactionCancellation->pf_name)
                            ->setCellValue('Z' . $indexStartLibsCancellations, $fetchTransactionCancellation->serie)
                            ->setCellValue('AA' . $indexStartLibsCancellations, $fetchTransactionCancellation->correlative)
                            ->setCellValue('AB' . $indexStartLibsCancellations, $fetchTransactionCancellation->annul_reason);
                        $indexStartLibsCancellations++;
                        $j++;
                    }
                }
            }

            $objWorkSheet->setCellValue('W' . ($indexStartLibsCancellations + 1), 'Total');
            $objWorkSheet->setCellValue('X' . ($indexStartLibsCancellations + 1), $totalCellAnnul);

            $objWorkSheet->getStyle("P2:P" . ($indexStartLibsCancellations + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("O2:O" . ($indexStartLibsCancellations + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("R2:R" . ($indexStartLibsCancellations + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("S2:S" . ($indexStartLibsCancellations + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("T2:T" . ($indexStartLibsCancellations + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("U2:U" . ($indexStartLibsCancellations + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("X2:X" . ($indexStartLibsCancellations + 1))->getAlignment()->setHorizontal('center');
            //$objWorkSheet->getStyle('Q1:Q2000')->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle('S1:S' . ($indexStartLibsCancellations + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle('T1:T' . ($indexStartLibsCancellations + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle('U1:U' . ($indexStartLibsCancellations + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle('V1:V' . ($indexStartLibsCancellations + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle('W1:W' . ($indexStartLibsCancellations + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle('X1:X' . ($indexStartLibsCancellations + 1))->getNumberFormat()->setFormatCode('#,##0.00');

            #OTRAS VENTAS
            $indexStartLibsOthersSales = 3;
            $l = 1;
            $totalCellOthers = 0;
            foreach ($salesTemp as $fetchTransactionOther) {
                if (in_array($fetchTransactionOther->prof, [3, 6, 8])) {
                    $totalCellOthers += $fetchTransactionOther->total;
                    $objWorkSheet->setCellValue('AE' . $indexStartLibsOthersSales, $l)
                        ->setCellValue('AF' . $indexStartLibsOthersSales, $fetchTransactionOther->date_added)
                        ->setCellValue('AG' . $indexStartLibsOthersSales, $fetchTransactionOther->cl_name)
                        ->setCellValue('AH' . $indexStartLibsOthersSales, $fetchTransactionOther->base)
                        ->setCellValue('AI' . $indexStartLibsOthersSales, $fetchTransactionOther->igv)
                        ->setCellValue('AJ' . $indexStartLibsOthersSales, $fetchTransactionOther->service)
                        ->setCellValue('AK' . $indexStartLibsOthersSales, $fetchTransactionOther->card)
                        ->setCellValue('AL' . $indexStartLibsOthersSales, $fetchTransactionOther->cash)
                        ->setCellValue('AM' . $indexStartLibsOthersSales, $fetchTransactionOther->total)
                        ->setCellValue('AN' . $indexStartLibsOthersSales, $fetchTransactionOther->pf_name)
                        ->setCellValue('AO' . $indexStartLibsOthersSales, $fetchTransactionOther->correlative);
                    $indexStartLibsOthersSales++;
                    $l++;
                }
            }

            $objWorkSheet->setCellValue('AL' . ($indexStartLibsOthersSales + 1), 'Total');
            $objWorkSheet->setCellValue('AM' . ($indexStartLibsOthersSales + 1), $totalCellOthers);

            $objWorkSheet->getStyle("AC2:AC" . ($indexStartLibsOthersSales + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("AF2:AF" . ($indexStartLibsOthersSales + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("AG2:AG" . ($indexStartLibsOthersSales + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("AH2:AH" . ($indexStartLibsOthersSales + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle("AI2:AI" . ($indexStartLibsOthersSales + 1))->getAlignment()->setHorizontal('center');
            $objWorkSheet->getStyle('AH1:AH' . ($indexStartLibsOthersSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle('AI1:AI' . ($indexStartLibsOthersSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle('AJ1:AJ' . ($indexStartLibsOthersSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle('AK1:AK' . ($indexStartLibsOthersSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle('AL1:AL' . ($indexStartLibsOthersSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');
            $objWorkSheet->getStyle('AM1:AM' . ($indexStartLibsOthersSales + 1))->getNumberFormat()->setFormatCode('#,##0.00');

            #TITULO DEL LIBRO
            $objWorkSheet->setTitle("" . $dataLocal[$i - 1]['name']);
        }

        $spreadsheet->setActiveSheetIndex(0);
        header("Access-Control-Allow-Origin:*");
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="CVENTAS_DE_' . $startExplode[0] . '_A_' . $endExplode[0] . '.xlsx"');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit();
    }

    public function exportSalesByPlate($dataLocal, $startExport, $endExport, $idArea, $idTypeSale, $idCategory, $idProf)
    {
        $host = $dataLocal['hostname'];
        $db = $dataLocal['database'];
        $user = $dataLocal['user'];
        $pass = $dataLocal['password'];
        $reason = $dataLocal['reason'];
        $ruc = $dataLocal['ruc'];
        $startExplode = explode(" ", $startExport);
        $endExplode = explode(" ", $endExport);
        date_default_timezone_set('Europe/Madrid');
        setlocale(LC_TIME, 'spanish');

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        #Estilos
        $sheet->getStyle('B4:L4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $sheet->getStyle('B4')->getFont()->setBold(false)->getColor()->setARGB("ffffff");
        $sheet->getStyle('B5:L5')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $sheet->getStyle('B5:L5')->getFont()->setBold(false)->getColor()->setARGB("ffffff");

        $sheet->getStyle("B4")->getAlignment()->setHorizontal('center');
        $sheet->getColumnDimension("B")->setAutoSize(false);
        $sheet->getColumnDimension("B")->setWidth('30');
        $sheet->getColumnDimension("C")->setAutoSize(false);
        $sheet->getColumnDimension("C")->setWidth('30');
        $sheet->getColumnDimension("D")->setAutoSize(false);
        $sheet->getColumnDimension("D")->setWidth('25');
        $sheet->getColumnDimension("E")->setAutoSize(false);
        $sheet->getColumnDimension("E")->setWidth('30');
        $sheet->getColumnDimension("F")->setAutoSize(false);
        $sheet->getColumnDimension("F")->setWidth('30');
        $sheet->getColumnDimension("G")->setAutoSize(false);
        $sheet->getColumnDimension("G")->setWidth('30');
        $sheet->getColumnDimension("H")->setAutoSize(false);
        $sheet->getColumnDimension("H")->setWidth('30');
        $sheet->getColumnDimension("I")->setAutoSize(false);
        $sheet->getColumnDimension("I")->setWidth('30');
        $sheet->getColumnDimension("J")->setAutoSize(false);
        $sheet->getColumnDimension("J")->setWidth('30');
        $sheet->getColumnDimension("K")->setAutoSize(false);
        $sheet->getColumnDimension("K")->setWidth('30');
        $sheet->getColumnDimension("L")->setAutoSize(false);
        $sheet->getColumnDimension("L")->setWidth('30');

        $sheet->getStyle('F6:F1000')->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('G6:G1000')->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('H6:H1000')->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('I6:I1000')->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('J6:J1000')->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle('K6:K1000')->getNumberFormat()->setFormatCode('#,##0.00');

        #CABECERA
        $sheet->setCellValue('B2', "Periodo: ");
        $sheet->setCellValue('C2', "$startExplode[0] a $endExplode[0]");
        $sheet->setCellValue('B4', "Reporte de ventas por plato");
        $sheet->mergeCells('B4:J4');
        $sheet->setCellValue('B5', "Plato / Bebida");
        $sheet->setCellValue('C5', "Categoría");
        $sheet->setCellValue('D5', "Comprobante");
        $sheet->setCellValue('E5', "Cantidad");
        $sheet->setCellValue('F5', "Precio (venta S/.)");
        $sheet->setCellValue('G5', "Costo Unitario (S/.)");
        $sheet->setCellValue('H5', "Costo Total (S/.)");
        $sheet->setCellValue('I5', "Margen de ganancia (S/.)");
        $sheet->setCellValue('J5', "Importe Pago (S/.)");
        $sheet->setCellValue('K5', "Descuento (S/.)");
        $sheet->setCellValue('L5', "Fecha Pago");

        #DATA
        $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
        $cone->query("SET NAMES 'utf8'");

        $WHERE = "";
        if ($idArea > 0) {
            $WHERE .= " AND pr.id_area = $idArea";
        }

        if ($idCategory > 0) {
            $WHERE .= " AND (pr.id_category = $idCategory OR ca.id_parent = $idCategory)";
        }

        if ($idProf > 0) {
            $WHERE .= " AND tr.id_proof = $idProf";
        }
        /*print_r("SELECT op.*,
               pp.name pp_name,pp.cost,
               CONCAT(pr.name,' - ',pp.name) pr_name,
               ca.name ca_name,
               pf.name pf_name,
               op.quantity num_sales,
               (op.price_total - op.price_promo) amount,
               tr.date_added trans_date_pay,
               cl.name cl_name
        FROM ordpros op
            INNER JOIN orders od ON od.id = op.id_order
            INNER JOIN users us ON us.id = od.id_user
            INNER JOIN propres pp ON pp.id = op.id_propre
            INNER JOIN products pr ON pr.id = pp.id_product
            INNER JOIN categories ca ON ca.id = pr.id_category
            INNER JOIN transactions tr ON tr.id = op.id_transaction
            INNER JOIN proofs pf ON pf.id = tr.id_proof
            LEFT JOIN clients cl ON cl.id = tr.id_client
        WHERE tr.state != 0 AND (tr.date_added BETWEEN '$startExport' AND '$endExport')
            $WHERE
        ORDER BY id_transaction DESC");
        exit();*/
        $fetchSalesByPlates = $cone->prepare("SELECT op.*,
               pp.name pp_name,pp.cost,
               CONCAT(pr.name,' - ',pp.name) pr_name,
               ca.name ca_name,
               pf.name pf_name,
               op.quantity num_sales,
               (op.price_total - op.price_promo) amount,
               tr.date_added trans_date_pay,
               cl.name cl_name
        FROM ordpros op
            INNER JOIN orders od ON od.id = op.id_order
            INNER JOIN users us ON us.id = od.id_user
            INNER JOIN propres pp ON pp.id = op.id_propre
            INNER JOIN products pr ON pr.id = pp.id_product
            INNER JOIN categories ca ON ca.id = pr.id_category
            INNER JOIN transactions tr ON tr.id = op.id_transaction
            INNER JOIN proofs pf ON pf.id = tr.id_proof
            LEFT JOIN clients cl ON cl.id = tr.id_client
        WHERE tr.state != 0 AND (tr.date_added BETWEEN '$startExport' AND '$endExport')
            $WHERE
        ORDER BY id_transaction DESC");
        $fetchSalesByPlates->execute();
        $index = 6;
        while ($fetchSalesByPlate = $fetchSalesByPlates->fetch(PDO::FETCH_OBJ)) {
            $sheet->setCellValue('B' . $index, $fetchSalesByPlate->pr_name);
            $sheet->setCellValue('C' . $index, $fetchSalesByPlate->ca_name);
            $sheet->setCellValue('D' . $index, $fetchSalesByPlate->pf_name);
            $sheet->setCellValue('E' . $index, $fetchSalesByPlate->num_sales);
            $sheet->setCellValue('F' . $index, $fetchSalesByPlate->price_unit);
            $sheet->setCellValue('G' . $index, $fetchSalesByPlate->cost_unit);
            $sheet->setCellValue('H' . $index, $fetchSalesByPlate->cost_total);
            $sheet->setCellValue('I' . $index, (100 - ($fetchSalesByPlate->cost_unit / $fetchSalesByPlate->price_unit) * 100) . " %");
            $sheet->setCellValue('J' . $index, $fetchSalesByPlate->amount);
            $sheet->setCellValue('K' . $index, $fetchSalesByPlate->price_promo);
            $sheet->setCellValue('L' . $index, $fetchSalesByPlate->trans_date_pay);
            $index++;
        }

        header("Access-Control-Allow-Origin:*");
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="RVENTAS_' . $startExplode[0] . '_AL_' . $endExplode[0] . '.xlsx"');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit();
    }

    public function getRealCostPropre($host, $db, $user, $pass, $id_propre, $id_ordpro, $return_html = false)
    {
        global $db;
        $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
        $cone->query("SET NAMES 'utf8'");

        $html_sus = '';
        $cost_total = 0;

        $SQL = "SELECT ps.*,
                   su.id    su_id,
                   su.name  su_name,
                   su.cost  su_cost,
                   
                   un1.id   un1_id,
                   un1.name un1_name,
                   un2.id   un2_id,
                   un2.name un2_name,
                   
                   COALESCE(ka.v_unit,su.cost) real_cost,
                   
                   ka.v_unit ka_v_unit
                   
            FROM propresups ps
            
              INNER JOIN supplies su ON su.id = ps.id_supply
                LEFT JOIN kardex ka ON ka.id_supply = su.id AND ka.type = 2 AND ka.id_ordpro = $id_ordpro
                
              LEFT JOIN unimeds un1 ON un1.id = su.id_unimed
              LEFT JOIN unimeds un2 ON un2.id = ps.id_unimed
              
            WHERE ps.id_propre = $id_propre
            GROUP BY ps.id";

        $sale = $cone->prepare($SQL);
        $sale->execute();

        while ($ps = $sale->fetch(PDO::FETCH_OBJ)) {

            // Obtener costo
            $SQL_1 = "SELECT *
                  FROM unimeds_rel
                  WHERE state = 1 AND (
                        (id_unimed_org = $ps->un1_id AND id_unimed_dst = $ps->un2_id) OR
                        (id_unimed_dst = $ps->un1_id AND id_unimed_org = $ps->un2_id)
                       )";
            $query = $cone->prepare($SQL_1);
            $query->execute();
            $cost = 0;

            $css_status = '';

            if ($ps->un1_id == $ps->un2_id || $ur = $query->fetch(PDO::FETCH_OBJ)) {
                // Son la misma unidad de medida, no se debe relacionar
                if ($ps->un1_id == $ps->un2_id) {

                    $cost = $ps->real_cost * $ps->quantity;
                } else if ($ur->id_unimed_org == $ps->un1_id) {

                    $cost = ($ps->real_cost * $ps->quantity) / $ur->quantity;
                } else {
                    $cost = ((($ur->quantity / $ps->quantity) / $ps->real_cost) * $ps->quantity) * $ps->quantity;
                }
            } else {
                $css_status = 'color:#ddd';
            }

            $cost_total += $cost;

            if ($return_html) {
                $html_sus .= '<tr style="' . $css_status . '">';
                $html_sus .= ' <td width="20px"> </td>';
                $html_sus .= ' <td>' . $ps->quantity . '</td>';
                $html_sus .= ' <td>' . $ps->un2_name . '</td>';
                $html_sus .= ' <td>' . $ps->su_id . ' : ' . $ps->su_name . ' (S/ <b>' . $ps->real_cost . '</b> : ' . $ps->su_cost . ' (' . $ps->ka_v_unit . ') x ' . $ps->un1_name . ')</td>';
                $html_sus .= ' <td>' . $cost . '</td>';
                $html_sus .= '</tr>';
            }
        }

        if ($return_html) {
            return '
                <table class="table table-bordered">
                    <tr>
                        <td colspan="4"><b> mmm - mmm</b></td>
                        <td><b>' . $cost_total . '</b></td>
                    </tr>
                    ' . $html_sus . '
                </table>
            ';
        } else {
            return $cost_total;
        }
    }

    public function most_selled($dataLocal, $start, $end)
    {
        $most_selled = [];
        $startExplode = explode(" ", $start);
        $endExplode = explode(" ", $end);
        $host = $dataLocal['hostname'];
        $db = $dataLocal['database'];
        $user = $dataLocal['user'];
        $pass = $dataLocal['password'];
        $cone = new PDO("mysql:host=$host;dbname=$db", "$user", "$pass");
        $cone->query("SET NAMES 'utf8'");
        // Mas vendidos
        $most_sales_queries = $cone->prepare("SELECT op.*,

        pp.id    pp_id,
        pp.name  pp_name,
        pp.cost  pp_cost,
        pp.price pp_price,
        
        pr.id    pr_id,
        pr.name  pr_name,
        
        ca.id        ca_id,
        ca.id_parent ca_id_parent,
        ca.name      ca_name,

        tr.pc_igv      tr_pc_igv,
        tr.pc_service  tr_pc_service
        
        FROM ordpros op
        INNER JOIN propres pp ON pp.id = op.id_propre
            INNER JOIN products pr ON pr.id = pp.id_product
            INNER JOIN categories ca ON ca.id = pr.id_category
        INNER JOIN transactions tr ON tr.id = op.id_transaction
            INNER JOIN regboxes rb ON rb.id = tr.id_regbox
        WHERE tr.id_branch = 1 AND tr.state != 0 AND tr.type = 1
            AND DATE(rb.date_added) BETWEEN '$startExplode[0]' AND '$endExplode[0]'");
        $most_sales_queries->execute();

        while ($o = $most_sales_queries->fetch(PDO::FETCH_OBJ)) {
            $key = 'pp_' . $o->pp_id;
            if (!isset($most_selled[$key])) {
                $_dt = [];
                $_dt['pr_name'] = $o->pr_name;
                $_dt['pp_name'] = $o->pp_name;
                $_dt['pp_price'] = $o->pp_price;
                $_dt['cost'] = 0;
                $_dt['price'] = 0;
                $_dt['base'] = 0;
                $_dt['igv'] = 0;
                $_dt['service'] = 0;
                $_dt['num_sales'] = 0;
                $_dt['margen_bruto'] = 0;
                $_dt['percent'] = 0;
                $most_selled[$key] = $_dt;
            }

            $cost_total = 0;

            $SQL = "SELECT ps.*,
                    su.id    su_id,
                    su.name  su_name,
                    su.cost  su_cost,
                    
                    un1.id   un1_id,
                    un1.name un1_name,
                    un2.id   un2_id,
                    un2.name un2_name,
                    
                    COALESCE(ka.v_unit,su.cost) real_cost,
                    
                    ka.v_unit ka_v_unit
                    
                FROM propresups ps
                
                INNER JOIN supplies su ON su.id = ps.id_supply
                    LEFT JOIN kardex ka ON ka.id_supply = su.id AND ka.type = 2 AND ka.id_ordpro = $o->id
                    
                LEFT JOIN unimeds un1 ON un1.id = su.id_unimed
                LEFT JOIN unimeds un2 ON un2.id = ps.id_unimed
                
                WHERE ps.id_propre = $o->pp_id
                GROUP BY ps.id";

            $sale = $cone->prepare($SQL);
            $sale->execute();

            while ($ps = $sale->fetch(PDO::FETCH_OBJ)) {

                // Obtener costo
                $SQL_1 = "SELECT *
                    FROM unimeds_rel
                    WHERE state = 1 AND (
                            (id_unimed_org = $ps->un1_id AND id_unimed_dst = $ps->un2_id) OR
                            (id_unimed_dst = $ps->un1_id AND id_unimed_org = $ps->un2_id)
                        )";
                $query = $cone->prepare($SQL_1);
                $query->execute();
                $cost = 0;

                $css_status = '';

                if ($ps->un1_id == $ps->un2_id || $ur = $query->fetch(PDO::FETCH_OBJ)) {
                    // Son la misma unidad de medida, no se debe relacionar
                    if ($ps->un1_id == $ps->un2_id) {
                        $cost = $ps->real_cost * $ps->quantity;
                    } else if ($ur->id_unimed_org == $ps->un1_id) {
                        $cost = ($ps->real_cost * $ps->quantity) / $ur->quantity;
                    } else {
                        $cost = ((($ur->quantity / $ps->quantity) / $ps->real_cost) * $ps->quantity) * $ps->quantity;
                    }
                } else {
                    $css_status = 'color:#ddd';
                }

                $cost_total += $cost;
            }

            $cost = $cost_total * $o->quantity;
            $price = $o->price_total;
            $base = ($price / (100 + $o->tr_pc_igv + $o->tr_pc_service)) * 100;
            $igv = ($base * $o->tr_pc_igv) / 100;
            $service = ($base * $o->tr_pc_service) / 100;
            $margen_bruto = $price - $cost - $igv /*- $service*/
            ;

            $om = &$most_selled[$key];
            $om['cost'] += $cost;
            $om['price'] += $price;
            $om['base'] += $base;
            $om['igv'] += $igv;
            $om['service'] += $service;
            $om['num_sales'] += $o->quantity;
            $om['margen_bruto'] += $margen_bruto;
        }

        $filter = array();
        foreach ($most_selled as $key => $row) {
            $filter[$key] = $row['num_sales'];
        }
        array_multisort($filter, SORT_DESC, $most_selled);

        date_default_timezone_set('Europe/Madrid');
        setlocale(LC_TIME, 'spanish');

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        #Estilos
        $sheet->getStyle('B4:L4')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $sheet->getStyle('B4')->getFont()->setBold(false)->getColor()->setARGB("ffffff");
        $sheet->getStyle('B5:L5')->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setARGB('002060');
        $sheet->getStyle('B5:L5')->getFont()->setBold(false)->getColor()->setARGB("ffffff");

        $sheet->getStyle("B4")->getAlignment()->setHorizontal('center');
        $sheet->getColumnDimension("B")->setAutoSize(false);
        $sheet->getColumnDimension("B")->setWidth('30');
        $sheet->getColumnDimension("C")->setAutoSize(false);
        $sheet->getColumnDimension("C")->setWidth('30');
        $sheet->getColumnDimension("D")->setAutoSize(false);
        $sheet->getColumnDimension("D")->setWidth('25');
        $sheet->getColumnDimension("E")->setAutoSize(false);
        $sheet->getColumnDimension("E")->setWidth('30');
        $sheet->getColumnDimension("F")->setAutoSize(false);
        $sheet->getColumnDimension("F")->setWidth('30');
        $sheet->getColumnDimension("G")->setAutoSize(false);
        $sheet->getColumnDimension("G")->setWidth('30');
        $sheet->getColumnDimension("H")->setAutoSize(false);
        $sheet->getColumnDimension("H")->setWidth('30');
        $sheet->getColumnDimension("I")->setAutoSize(false);
        $sheet->getColumnDimension("I")->setWidth('30');
        $sheet->getColumnDimension("J")->setAutoSize(false);
        $sheet->getColumnDimension("J")->setWidth('30');
        $sheet->getColumnDimension("K")->setAutoSize(false);
        $sheet->getColumnDimension("K")->setWidth('30');
        $sheet->getColumnDimension("L")->setAutoSize(false);
        $sheet->getColumnDimension("L")->setWidth('30');

        #CABECERA
        $sheet->setCellValue('B2', "Periodo: ");
        $sheet->setCellValue('C2', "$startExplode[0] a $endExplode[0]");
        $sheet->setCellValue('B4', "Reporte de platos mas vendidos");
        $sheet->mergeCells('B4:J4');
        $sheet->setCellValue('B5', "Plato / Bebida");
        $sheet->setCellValue('C5', "Costo");
        $sheet->setCellValue('D5', "Precio Total");
        $sheet->setCellValue('E5', "Base");
        $sheet->setCellValue('F5', "IGV");
        $sheet->setCellValue('G5', "Servicios");
        $sheet->setCellValue('H5', "Numero de Ventas");
        $sheet->setCellValue('I5', "Precio del Plato Promedio");
        $sheet->setCellValue('J5', "Precio actual del plato");
        $sheet->setCellValue('K5', "Margen de Ganancia S/.");
        $sheet->setCellValue('L5', "Margen de Ganancia %");

        /*print_r($most_selled);
        exit();*/

        $index = 6;
        foreach ($most_selled as $item) {
            $sheet->setCellValue('B' . $index, $item['pr_name'] . ' ' . $item['pp_name']);
            $sheet->setCellValue('C' . $index, $item['cost']);
            $sheet->setCellValue('D' . $index, $item['price']);
            $sheet->setCellValue('E' . $index, $item['base']);
            $sheet->setCellValue('F' . $index, $item['igv']);
            $sheet->setCellValue('G' . $index, $item['service']);
            $sheet->setCellValue('H' . $index, $item['num_sales']);
            $sheet->setCellValue('I' . $index, ($item['price'] / $item['num_sales']));
            $sheet->setCellValue('J' . $index, $item['pp_price']);
            $sheet->setCellValue('K' . $index, $item['margen_bruto']);
            $sheet->setCellValue('L' . $index, $item['percent']);
            $index++;
        }

        $sheet->getStyle("C6:C$index")->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle("D6:D$index")->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle("E6:E$index")->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle("F6:F$index")->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle("G6:G$index")->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle("I6:I$index")->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle("J6:J$index")->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle("K6:K$index")->getNumberFormat()->setFormatCode('#,##0.00');
        $sheet->getStyle("L6:L$index")->getNumberFormat()->setFormatCode('#,##0.00');

        header("Access-Control-Allow-Origin:*");
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="RTOP_' . $startExplode[0] . '_AL_' . $endExplode[0] . '.xlsx"');
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
        exit();
    }
}
